# -*- coding: utf-8 -*-

import hashlib
import json
import math
import operator as op
import random
import re
import string
from collections import namedtuple
from copy import copy
from functools import reduce
from itertools import combinations, permutations, combinations_with_replacement, chain
from typing import List, TypeVar, Iterator, Dict, Union

from recordclass import recordclass


def day1_part1(in_: str) -> None:
    lvl = 0
    for c in in_:
        if c == '(':
            lvl += 1
        elif c == ')':
            lvl -= 1
        else:
            assert False
    print(lvl)


def day1_part2(in_: str) -> None:
    i = 1
    lvl = 0
    for c in in_:
        if c == '(':
            lvl += 1
        elif c == ')':
            lvl -= 1
            if lvl < 0:
                break
        else:
            assert False
        i += 1
    print(i)


# noinspection PyTypeChecker
def day2_part1(in_: str) -> None:
    lines = in_.splitlines()
    tuples = map(lambda l: map(int, l.split('x')), lines)
    combines = map(lambda l: combinations(l, 2), tuples)
    faces = map(lambda l: list(map(lambda x: x[0] * x[1], l)), combines)
    areas = map(lambda l: 2 * l + [min(l)], faces)
    totals = map(sum, areas)
    print(sum(totals))


def day2_part2(in_: str) -> None:
    lines = in_.splitlines()
    tuples = tuple(map(lambda l: list(map(int, l.split('x'))), lines))
    combines = map(lambda l: combinations(l, 2), tuples)
    perims = map(lambda l: list(map(lambda x: 2 * x[0] + 2 * x[1], l)), combines)
    totals = map(lambda l: min(l[0]) + reduce(op.mul, l[1], 1), zip(perims, tuples))
    print(sum(totals))


def day3_part1(in_: str) -> None:
    x = y = 0
    visited = {(x, y): 1}

    for c in in_:
        if c == '^':
            y -= 1
        elif c == 'v':
            y += 1
        elif c == '>':
            x += 1
        elif c == '<':
            x -= 1

        try:
            visited[(x, y)] += 1
        except KeyError:
            visited[(x, y)] = 1

    print(len(visited))


def day3_part2(in_: str) -> None:
    santa_x = santa_y = 0
    robo_x = robo_y = 0
    santas_turn = True
    visited = {(santa_x, santa_y): 2}

    for c in in_:
        if c == '^':
            if santas_turn:
                santa_y -= 1
            else:
                robo_y -= 1
        elif c == 'v':
            if santas_turn:
                santa_y += 1
            else:
                robo_y += 1
        elif c == '>':
            if santas_turn:
                santa_x += 1
            else:
                robo_x += 1
        elif c == '<':
            if santas_turn:
                santa_x -= 1
            else:
                robo_x -= 1

        try:
            if santas_turn:
                visited[(santa_x, santa_y)] += 1
            else:
                visited[(robo_x, robo_y)] += 1
        except KeyError:
            if santas_turn:
                visited[(santa_x, santa_y)] = 1
            else:
                visited[(robo_x, robo_y)] = 1

        santas_turn = not santas_turn

    print(len(visited))


def day4_part1(in_: str) -> None:
    key = in_
    i = 1
    hashed = ''

    while not hashed.startswith('00000'):
        i += 1
        hashed = hashlib.md5(f'{key}{i}'.encode()).hexdigest()

    print(i)


def day4_part2(in_: str) -> None:
    key = in_
    i = 1
    hashed = ''

    while not hashed.startswith('000000'):
        i += 1
        hashed = hashlib.md5(f'{key}{i}'.encode()).hexdigest()

    print(i)


def day5_part1(in_: str) -> None:
    r_vowels = re.compile(r'.*[aeiou].*[aeiou].*[aeiou].*')
    r_double = re.compile(r'.*(?P<char>.)(?P=char).*')
    r_specials = re.compile(r'.*(ab|cd|pq|xy).*')

    n = 0
    for line in in_.splitlines():
        if r_vowels.match(line) is not None and \
                r_double.match(line) is not None and \
                r_specials.match(line) is None:
            n += 1

    print(n)


def day5_part2(in_: str) -> None:
    r_pair = re.compile(r'.*(?P<pair>..).*(?P=pair).*')
    r_repeats = re.compile(r'.*(?P<char>.).(?P=char).*')

    n = 0
    for line in in_.splitlines():
        if r_pair.match(line) is not None and \
                r_repeats.match(line) is not None:
            n += 1

    print(n)


def day6_part1(in_: str) -> None:
    r_line = re.compile(r'(?P<cmd>turn (?:on|off)|toggle) (?P<x1>\d+),(?P<y1>\d+) through (?P<x2>\d+),(?P<y2>\d+)')
    grid = [[False for _ in range(1000)] for _ in range(1000)]

    for line in in_.splitlines():
        m = r_line.match(line)
        for y in range(int(m.group('y1')), int(m.group('y2')) + 1):
            for x in range(int(m.group('x1')), int(m.group('x2')) + 1):
                if m.group('cmd') == 'turn on':
                    grid[y][x] = True
                elif m.group('cmd') == 'turn off':
                    grid[y][x] = False
                elif m.group('cmd') == 'toggle':
                    grid[y][x] = not grid[y][x]
                else:
                    assert False

    print(sum(map(lambda l: sum(l), grid)))


def day6_part2(in_: str) -> None:
    r_line = re.compile(r'(?P<cmd>turn (?:on|off)|toggle) (?P<x1>\d+),(?P<y1>\d+) through (?P<x2>\d+),(?P<y2>\d+)')
    grid = [[0 for _ in range(1000)] for _ in range(1000)]

    for line in in_.splitlines():
        m = r_line.match(line)
        for y in range(int(m.group('y1')), int(m.group('y2')) + 1):
            for x in range(int(m.group('x1')), int(m.group('x2')) + 1):
                if m.group('cmd') == 'turn on':
                    grid[y][x] += 1
                elif m.group('cmd') == 'turn off':
                    if grid[y][x] > 0:
                        grid[y][x] -= 1
                elif m.group('cmd') == 'toggle':
                    grid[y][x] += 2
                else:
                    assert False

    print(sum(map(lambda l: sum(l), grid)))


def day7_part1(in_: str) -> None:
    rs_id = r'[a-z]+'
    rs_val = rf'{rs_id}|[0-9]+'
    r_wire = re.compile(rf'(?:({rs_val}) )?(?:(AND|OR|RSHIFT|LSHIFT|NOT) )?({rs_val}) -> (?P<out>{rs_id})')

    # noinspection PyPep8Naming
    Wire = recordclass('Wire', ['left', 'op', 'right', 'out', 'value'])
    wires = dict()

    for line in in_.splitlines():
        m = r_wire.match(line)
        wires[m.group('out')] = Wire(*m.groups(), None)

    def get_value(arg: str) -> int:
        try:
            return int(arg)
        except ValueError:
            pass

        wire = wires[arg]
        if wire.value is None:
            if wire.op == 'AND':
                wire.value = get_value(wire.left) & get_value(wire.right)
            elif wire.op == 'OR':
                wire.value = get_value(wire.left) | get_value(wire.right)
            elif wire.op == 'LSHIFT':
                wire.value = get_value(wire.left) << get_value(wire.right)
            elif wire.op == 'RSHIFT':
                wire.value = get_value(wire.left) >> get_value(wire.right)
            elif wire.op == 'NOT':
                wire.value = ~ get_value(wire.right)
            elif wire.op is None:
                wire.value = get_value(wire.right)
            else:
                assert False

        return wire.value

    print(get_value('a'))


def day7_part2(in_: str) -> None:
    rs_id = r'[a-z]+'
    rs_val = rf'{rs_id}|[0-9]+'
    r_wire = re.compile(rf'(?:({rs_val}) )?(?:(AND|OR|RSHIFT|LSHIFT|NOT) )?({rs_val}) -> (?P<out>{rs_id})')

    # noinspection PyPep8Naming
    Wire = recordclass('Wire', ['left', 'op', 'right', 'out', 'value'])
    wires = dict()

    for line in in_.splitlines():
        m = r_wire.match(line)
        wires[m.group('out')] = Wire(*m.groups(), None)

    def get_value(arg: str) -> int:
        try:
            return int(arg)
        except ValueError:
            pass

        wire = wires[arg]
        if wire.value is None:
            if wire.op == 'AND':
                wire.value = get_value(wire.left) & get_value(wire.right)
            elif wire.op == 'OR':
                wire.value = get_value(wire.left) | get_value(wire.right)
            elif wire.op == 'LSHIFT':
                wire.value = get_value(wire.left) << get_value(wire.right)
            elif wire.op == 'RSHIFT':
                wire.value = get_value(wire.left) >> get_value(wire.right)
            elif wire.op == 'NOT':
                wire.value = ~ get_value(wire.right)
            elif wire.op is None:
                wire.value = get_value(wire.right)
            else:
                assert False

        return wire.value

    last_a = get_value('a')
    for w in wires.values():
        w.value = None
    wires['b'].value = last_a
    print(get_value('a'))


def day8_part1(in_: str) -> None:
    n_lit = 0
    n_mem = 0
    for line in in_.splitlines():
        n_lit += len(line)
        i = 0
        while i < len(line):
            if line[i] != '"':
                n_mem += 1
            if line[i] == '\\':
                c = line[i + 1]
                if c == '\\' or c == '"':
                    i += 1
                elif c == 'x':
                    i += 3
                else:
                    assert False
            i += 1
    print(n_lit - n_mem)


def day8_part2(in_: str) -> None:
    n_orig = 0
    n_new = 0
    for line in in_.splitlines():
        n_orig += len(line)
        n_new += 2
        for c in line:
            n_new += 1
            if c == '\\' or c == '"':
                n_new += 1
    print(n_new - n_orig)


def day9_part1(in_: str) -> None:
    r_line = re.compile(r'(?P<from>[A-Za-z]+) to (?P<to>[A-Za-z]+) = (?P<dist>[0-9]+)')

    cities = set()
    distances = {}

    for line in in_.splitlines():
        m = r_line.match(line)
        cities.add(m.group('from'))
        cities.add(m.group('to'))
        distances[(m.group('from'), m.group('to'))] = int(m.group('dist'))
        distances[(m.group('to'), m.group('from'))] = int(m.group('dist'))

    routes = permutations(cities, len(cities))
    shortest = math.inf

    for route in routes:
        route_dist = 0
        for i in range(len(route) - 1):
            route_dist += distances[route[i:i + 2]]
        shortest = min(shortest, route_dist)

    print(shortest)


def day9_part2(in_: str) -> None:
    r_line = re.compile(r'(?P<from>[A-Za-z]+) to (?P<to>[A-Za-z]+) = (?P<dist>[0-9]+)')

    cities = set()
    distances = {}

    for line in in_.splitlines():
        m = r_line.match(line)
        cities.add(m.group('from'))
        cities.add(m.group('to'))
        distances[(m.group('from'), m.group('to'))] = int(m.group('dist'))
        distances[(m.group('to'), m.group('from'))] = int(m.group('dist'))

    routes = permutations(cities, len(cities))
    longest = - math.inf

    for route in routes:
        route_dist = 0
        for i in range(len(route) - 1):
            route_dist += distances[route[i:i + 2]]
        longest = max(longest, route_dist)

    print(longest)


def day10_part1(in_: str) -> None:
    def next_lookandsay(val: int) -> int:
        try:
            int(val)
        except (TypeError, ValueError):
            assert False

        next_val = ''
        last_c = None
        n_c = 0
        for c in str(val):
            if c != last_c:
                if last_c is not None:
                    next_val += f'{n_c}{last_c}'
                last_c = c
                n_c = 1
            else:
                n_c += 1

        next_val += f'{n_c}{last_c}'
        return next_val

    cur = in_
    for _ in range(40):
        cur = next_lookandsay(cur)

    print(len(cur))


def day10_part2(in_: str) -> None:
    def next_lookandsay(val: int) -> int:
        try:
            int(val)
        except (TypeError, ValueError):
            assert False

        next_val = ''
        last_c = None
        n_c = 0
        for c in str(val):
            if c != last_c:
                if last_c is not None:
                    next_val += f'{n_c}{last_c}'
                last_c = c
                n_c = 1
            else:
                n_c += 1

        next_val += f'{n_c}{last_c}'
        return next_val

    cur = in_
    for i in range(50):
        cur = next_lookandsay(cur)

    print(len(cur))


def day11_part1(in_: str) -> None:
    alpha = string.ascii_lowercase
    rs_incr_three = '|'.join([alpha[i:i + 3] for i in range(len(alpha) - 2)])
    r_three = re.compile(rf'.*({rs_incr_three}).*')
    r_letters = re.compile(r'.*[iol].*')
    r_pairs = re.compile(r'.*(?P<one>.)(?P=one).*(?P<two>.)(?P=two).*')

    def next_alpha(s: str) -> str:
        if s == '':
            return 'a'

        try:
            return s[:-1] + alpha[alpha.index(s[-1]) + 1]
        except IndexError:
            return next_alpha(s[:-1]) + 'a'

    password = in_
    while True:
        if r_three.match(password) is not None and r_letters.match(password) is None and r_pairs.match(
                password) is not None:
            break
        password = next_alpha(password)
    print(password)


def day11_part2(in_: str) -> None:
    alpha = string.ascii_lowercase
    rs_incr_three = '|'.join([alpha[i:i + 3] for i in range(len(alpha) - 2)])
    r_three = re.compile(rf'.*({rs_incr_three}).*')
    r_letters = re.compile(r'.*[iol].*')
    r_pairs = re.compile(r'.*(?P<one>.)(?P=one).*(?P<two>.)(?P=two).*')

    def next_alpha(s: str) -> str:
        if s == '':
            return 'a'

        try:
            return s[:-1] + alpha[alpha.index(s[-1]) + 1]
        except IndexError:
            return next_alpha(s[:-1]) + 'a'

    one_find = False
    password = in_
    while True:
        if r_three.match(password) is not None and r_letters.match(password) is None and r_pairs.match(
                password) is not None:
            if one_find:
                break
            one_find = True
        password = next_alpha(password)
    print(password)


def day12_part1(in_: str) -> None:
    def json_sum(json_obj) -> int:
        if isinstance(json_obj, list):
            out = 0
            for o in json_obj:
                out += json_sum(o)
            return out
        elif isinstance(json_obj, dict):
            out = 0
            for o in json_obj.values():
                out += json_sum(o)
            return out
        elif isinstance(json_obj, int):
            return json_obj
        return 0

    print(json_sum(json.loads(in_)))


def day12_part2(in_: str) -> None:
    def json_sum(json_obj) -> int:
        if isinstance(json_obj, list):
            out = 0
            for o in json_obj:
                out += json_sum(o)
            return out
        elif isinstance(json_obj, dict):
            out = 0
            for o in json_obj.values():
                if o == 'red':
                    return 0
                out += json_sum(o)
            return out
        elif isinstance(json_obj, int):
            return json_obj
        return 0

    print(json_sum(json.loads(in_)))


def day13_part1(in_: str) -> None:
    r_line = re.compile(r'(?P<left>[A-Za-z]+) would (?P<sign>gain|lose) (?P<val>[0-9]+) happiness units '
                        r'by sitting next to (?P<right>[A-Za-z]+).')

    beings = set()
    happiness = {}

    for line in in_.splitlines():
        m = r_line.match(line)
        beings.add(m.group('left'))
        beings.add(m.group('right'))
        sign = 1 if m.group('sign') == 'gain' else -1
        happiness[(m.group('left'), m.group('right'))] = int(m.group('val')) * sign

    tables = permutations(beings, len(beings))
    happier = - math.inf

    for table in tables:
        table_hap = 0
        for i in range(len(table)):
            try:
                left = table[i - 1]
            except IndexError:
                left = table[-1]
            try:
                right = table[i + 1]
            except IndexError:
                right = table[0]
            table_hap += happiness[(table[i], left)]
            table_hap += happiness[(table[i], right)]
        happier = max(happier, table_hap)

    print(happier)


def day13_part2(in_: str) -> None:
    r_line = re.compile(r'(?P<left>[A-Za-z]+) would (?P<sign>gain|lose) (?P<val>[0-9]+) happiness units '
                        r'by sitting next to (?P<right>[A-Za-z]+).')

    beings = set()
    happiness = {}

    for line in in_.splitlines():
        m = r_line.match(line)
        beings.add(m.group('left'))
        beings.add(m.group('right'))
        sign = 1 if m.group('sign') == 'gain' else -1
        happiness[(m.group('left'), m.group('right'))] = int(m.group('val')) * sign

    for being in beings:
        happiness[('__MYSELF__', being)] = 0
        happiness[(being, '__MYSELF__')] = 0
    beings.add('__MYSELF__')

    tables = permutations(beings, len(beings))
    happier = - math.inf

    for table in tables:
        table_hap = 0
        for i in range(len(table)):
            try:
                left = table[i - 1]
            except IndexError:
                left = table[-1]
            try:
                right = table[i + 1]
            except IndexError:
                right = table[0]
            table_hap += happiness[(table[i], left)]
            table_hap += happiness[(table[i], right)]
        happier = max(happier, table_hap)

    print(happier)


def day14_part1(in_: str) -> None:
    r_line = re.compile(r'[A-Za-z]+ can fly (?P<kms>[0-9]+) km/s for (?P<fly_sec>[0-9]+) seconds, '
                        r'but then must rest for (?P<rest_sec>[0-9]+) seconds.')

    longest = 0
    for line in in_.splitlines():
        m = r_line.match(line)

        distance = 0
        time = 2503
        while time > 0:
            elapsed = min(int(m.group('fly_sec')), time)
            time -= elapsed
            distance += elapsed * int(m.group('kms'))
            time -= int(m.group('rest_sec'))

        longest = max(longest, distance)

    print(longest)


def day14_part2(in_: str) -> None:
    r_line = re.compile(r'[A-Za-z]+ can fly (?P<kms>[0-9]+) km/s for (?P<fly_sec>[0-9]+) seconds, '
                        r'but then must rest for (?P<rest_sec>[0-9]+) seconds.')

    # noinspection PyPep8Naming
    Reindeer = recordclass('Reindeer', ['kms', 'fly_sec', 'rest_sec', 'points'])
    reindeers = []

    for line in in_.splitlines():
        m = r_line.match(line)
        reindeers.append(Reindeer(*[int(g) for g in m.groups()], 0))

    def get_distance(time: int, reindeer: Reindeer) -> int:
        distance = 0
        while time > 0:
            elapsed = min(reindeer.fly_sec, time)
            time -= elapsed
            distance += elapsed * reindeer.kms
            time -= reindeer.rest_sec
        return distance

    for i in range(1, 2504):
        distances = []
        for r in reindeers:
            distances.append(get_distance(i, r))
        maximum = max(distances)
        for r, d in zip(reindeers, distances):
            if d == maximum:
                r.points += 1

    print(max(map(lambda x: x.points, reindeers)))


def day15_part1(in_: str) -> None:
    rs_i = r'(-?[0-9]+)'
    r_line = re.compile(
        rf'(?P<name>[a-z]+): capacity {rs_i}, durability {rs_i}, flavor {rs_i}, texture {rs_i}, calories {rs_i}',
        flags=re.IGNORECASE)

    Ingredient = namedtuple('Ingredient', ['capacity', 'durability', 'flavor', 'texture', 'calories'])
    ingrs = {}
    for line in in_.splitlines():
        m = r_line.match(line)
        ingrs[m.group('name')] = Ingredient(*[int(x) for x in m.groups()[1:]])

    max_score = 0
    for comb in combinations_with_replacement(ingrs.keys(), 100):
        score = 1
        for prop in Ingredient._fields[0:-1]:
            total = 0
            for ingr in ingrs.keys():
                total += comb.count(ingr) * getattr(ingrs[ingr], prop)
            score *= max(total, 0)
        max_score = max(max_score, score)

    print(max_score)


def day15_part2(in_: str) -> None:
    rs_i = r'(-?[0-9]+)'
    r_line = re.compile(
        rf'(?P<name>[a-z]+): capacity {rs_i}, durability {rs_i}, flavor {rs_i}, texture {rs_i}, calories {rs_i}',
        flags=re.IGNORECASE)

    Ingredient = namedtuple('Ingredient', ['capacity', 'durability', 'flavor', 'texture', 'calories'])
    ingrs = {}
    for line in in_.splitlines():
        m = r_line.match(line)
        ingrs[m.group('name')] = Ingredient(*[int(x) for x in m.groups()[1:]])

    max_score = 0
    for comb in combinations_with_replacement(ingrs.keys(), 100):
        calories = 0
        for ingr in ingrs.keys():
            calories += comb.count(ingr) * getattr(ingrs[ingr], 'calories')
        if calories != 500:
            continue

        score = 1
        for prop in Ingredient._fields[0:-1]:
            total = 0
            for ingr in ingrs.keys():
                total += comb.count(ingr) * getattr(ingrs[ingr], prop)
            score *= max(total, 0)
        max_score = max(max_score, score)

    print(max_score)


def day16_part1(in_: str) -> None:
    r_line = re.compile(r'Sue (?P<id>[0-9]+): (?P<attrs>(?:[a-z]+: [0-9]+, )*[a-z]+: [0-9]+)')

    target = {
        'children': 3,
        'cats': 7,
        'samoyeds': 2,
        'pomeranians': 3,
        'akitas': 0,
        'vizslas': 0,
        'goldfish': 5,
        'trees': 3,
        'cars': 2,
        'perfumes': 1
    }

    for line in in_.splitlines():
        m = r_line.match(line)

        for attr in m.group('attrs').split(','):
            k, v = attr.split(':')
            if int(v) != target[k.strip()]:
                break
        else:
            print(m.group('id'))


def day16_part2(in_: str) -> None:
    r_line = re.compile(r'Sue (?P<id>[0-9]+): (?P<attrs>(?:[a-z]+: [0-9]+, )*[a-z]+: [0-9]+)')

    target = {
        'children': 3,
        'cats': 7,
        'samoyeds': 2,
        'pomeranians': 3,
        'akitas': 0,
        'vizslas': 0,
        'goldfish': 5,
        'trees': 3,
        'cars': 2,
        'perfumes': 1
    }

    for line in in_.splitlines():
        m = r_line.match(line)

        for attr in m.group('attrs').split(','):
            k, v = attr.split(':')
            k = k.strip()
            if k == 'cats' or k == 'trees':
                if int(v) <= target[k]:
                    break
            elif k == 'pomeranians' or k == 'goldfish':
                if int(v) >= target[k]:
                    break
            else:
                if int(v) != target[k]:
                    break
        else:
            print(m.group('id'))
            # return


def day17_part1(in_: str) -> None:
    containers = list(map(int, in_.splitlines()))
    total = 0
    for i in range(len(containers)):
        for comb in combinations(containers, i):
            if sum(comb) == 150:
                total += 1
    print(total)


def day17_part2(in_: str) -> None:
    containers = list(map(int, in_.splitlines()))
    for i in range(len(containers)):
        total = 0
        for comb in combinations(containers, i):
            if sum(comb) == 150:
                total += 1
        if total > 0:
            print(total)
            return


def day18_part1(in_: str) -> None:
    grid = [[c == '#' for c in line] for line in in_.splitlines()]

    def get(g: list, x_ind: int, y_ind: int) -> bool:
        try:
            if x_ind < 0 or y_ind < 0:
                raise IndexError
            return g[y_ind][x_ind]
        except IndexError:
            return False

    for _ in range(100):
        newgrid = []
        for y in range(len(grid)):
            newgrid.append([])
            for x in range(len(grid[y])):
                neighbors = sum([get(grid, i, j) for j in range(y - 1, y + 2)
                                 for i in range(x - 1, x + 2) if j != y or i != x])

                if grid[y][x]:
                    newgrid[-1].append(neighbors == 2 or neighbors == 3)
                else:
                    newgrid[-1].append(neighbors == 3)
        grid = newgrid

    print(sum([sum(line) for line in grid]))


def day18_part2(in_: str) -> None:
    grid = [[c == '#' for c in line] for line in in_.splitlines()]

    def get(g: list, x_ind: int, y_ind: int) -> bool:
        try:
            if x_ind < 0 or y_ind < 0:
                raise IndexError
            return g[y_ind][x_ind]
        except IndexError:
            return False

    for _ in range(100):
        newgrid = []
        for y in range(len(grid)):
            newgrid.append([])
            for x in range(len(grid[y])):
                if (x == 0 and y == 0) or (x == 0 and y == len(grid) - 1) or \
                        (x == len(grid[y]) - 1 and y == 0) or (x == len(grid[y]) - 1 and y == len(grid) - 1):
                    newgrid[-1].append(True)
                    continue

                neighbors = sum([get(grid, i, j) for j in range(y - 1, y + 2)
                                 for i in range(x - 1, x + 2) if j != y or i != x])

                if grid[y][x]:
                    newgrid[-1].append(neighbors == 2 or neighbors == 3)
                else:
                    newgrid[-1].append(neighbors == 3)
        grid = newgrid

    print(sum([sum(line) for line in grid]))


def day19_part1(in_: str) -> None:
    replacements, molecule = in_.split('\n\n')
    molecules = set()

    for line in replacements.splitlines():
        bef, aft = line.split(' => ')
        try:
            ind = molecule.index(bef)
            while True:
                molecules.add(f'{molecule[:ind]}{aft}{molecule[ind + len(bef):]}')
                ind = molecule.index(bef, ind + len(bef))
        except ValueError:
            pass

    print(len(molecules))


def day19_part2(in_: str) -> None:
    body, molecule = in_.split('\n\n')
    replacements = [line.split(' => ') for line in body.splitlines()]

    min_count = math.inf
    while True:
        try:
            count = 0
            new_molecule = molecule
            while new_molecule != 'e':
                tmp = replacements.copy()
                random.shuffle(tmp)
                for bef, aft in tmp:
                    if aft in new_molecule:
                        new_molecule = new_molecule.replace(aft, bef, 1)
                        count += 1
                        break
                else:
                    raise ValueError

            if min(min_count, count) < min_count:
                min_count = min(min_count, count)
                print(min_count)
        except ValueError:
            pass


def day20_part1(in_: str) -> None:
    def divisors_of(n: int) -> List[int]:
        for i in range(1, int(math.sqrt(n) + 1)):
            if n % i == 0:
                yield i
                if i * i != n:
                    yield n / i

    trgt = int(in_)
    house = 1
    while True:
        if sum(divisors_of(house)) * 10 >= trgt:
            break
        house += 1
    print(house)


def day20_part2(in_: str) -> None:
    def divisors_of(n: int) -> List[int]:
        for i in range(1, int(math.sqrt(n) + 1)):
            if n % i == 0:
                yield i
                if i * i != n:
                    yield n / i

    trgt = int(in_)
    house = 1
    while True:
        if sum(filter(lambda x: house / x <= 50, divisors_of(house))) * 11 >= trgt:
            break
        house += 1
    print(house)


# noinspection PyTypeChecker
def day21_part1(in_: str) -> None:
    # noinspection PyPep8Naming
    Character = recordclass('Character', ['hit_points', 'damage', 'armor'])
    Item = namedtuple('Item', ['name', 'cost', 'damage', 'armor'])

    weapons = [
        Item('Dagger', 8, 4, 0),
        Item('Shortsword', 10, 5, 0),
        Item('Warhammer', 25, 6, 0),
        Item('Longsword', 40, 7, 0),
        Item('Greataxe', 74, 8, 0)
    ]
    armors = [
        Item('Leather', 13, 0, 1),
        Item('Chainmail', 31, 0, 2),
        Item('Splintmail', 53, 0, 3),
        Item('Bandedmail', 75, 0, 4),
        Item('Platemail', 102, 0, 5)
    ]
    rings = [
        Item('Damage +1', 25, 1, 0),
        Item('Damage +2', 50, 2, 0),
        Item('Damage +3', 100, 3, 0),
        Item('Defense +1', 20, 0, 1),
        Item('Defense +2', 40, 0, 2),
        Item('Defense +3', 80, 0, 3)
    ]

    base_boss = Character(*list(map(lambda x: int(x.split()[-1]), in_.splitlines())))

    least_amount = math.inf
    for weapon in weapons:
        for armor in armors + [None]:
            for ring1, ring2 in chain([[None, None]], combinations(rings + [None], 2)):
                amount = sum([x.cost for x in [weapon, armor, ring1, ring2] if x is not None])
                if amount >= least_amount:
                    continue

                player = Character(100,
                                   sum([x.damage for x in [weapon, armor, ring1, ring2] if x is not None]),
                                   sum([x.armor for x in [weapon, armor, ring1, ring2] if x is not None]))

                boss = copy(base_boss)
                playing = (player, boss)
                while player.hit_points > 0 and boss.hit_points > 0:
                    playing[1].hit_points -= max(1, playing[0].damage - playing[1].armor)
                    playing = playing[::-1]

                if player.hit_points > 0:
                    least_amount = min(least_amount, amount)
    print(least_amount)


# noinspection PyTypeChecker
def day21_part2(in_: str) -> None:
    # noinspection PyPep8Naming
    Character = recordclass('Character', ['hit_points', 'damage', 'armor'])
    Item = namedtuple('Item', ['name', 'cost', 'damage', 'armor'])

    weapons = [
        Item('Dagger', 8, 4, 0),
        Item('Shortsword', 10, 5, 0),
        Item('Warhammer', 25, 6, 0),
        Item('Longsword', 40, 7, 0),
        Item('Greataxe', 74, 8, 0)
    ]
    armors = [
        Item('Leather', 13, 0, 1),
        Item('Chainmail', 31, 0, 2),
        Item('Splintmail', 53, 0, 3),
        Item('Bandedmail', 75, 0, 4),
        Item('Platemail', 102, 0, 5)
    ]
    rings = [
        Item('Damage +1', 25, 1, 0),
        Item('Damage +2', 50, 2, 0),
        Item('Damage +3', 100, 3, 0),
        Item('Defense +1', 20, 0, 1),
        Item('Defense +2', 40, 0, 2),
        Item('Defense +3', 80, 0, 3)
    ]

    base_boss = Character(*list(map(lambda x: int(x.split()[-1]), in_.splitlines())))

    most_amount = 0
    for weapon in weapons:
        for armor in armors + [None]:
            for ring1, ring2 in chain([[None, None]], combinations(rings + [None], 2)):
                amount = sum([x.cost for x in [weapon, armor, ring1, ring2] if x is not None])

                player = Character(100,
                                   sum([x.damage for x in [weapon, armor, ring1, ring2] if x is not None]),
                                   sum([x.armor for x in [weapon, armor, ring1, ring2] if x is not None]))

                boss = copy(base_boss)
                playing = (player, boss)
                while player.hit_points > 0 and boss.hit_points > 0:
                    playing[1].hit_points -= max(1, playing[0].damage - playing[1].armor)
                    playing = playing[::-1]

                if boss.hit_points > 0:
                    most_amount = max(most_amount, amount)
    print(most_amount)


# noinspection PyPep8Naming
def day22_part1(in_: str) -> None:
    Player = recordclass(
        'Player',
        ['hit_points', 'mana_points', 'mana_spent', 'shield_remain', 'poison_remain', 'recharge_remain'])
    Boss = recordclass('Boss', ['hit_points', 'damage'])

    base_player = Player(50, 500, 0, 0, 0, 0)
    base_boss = Boss(*list(map(lambda x: int(x.split()[-1]), in_.splitlines())))

    Spell = recordclass('Spell', ['cost', 'func'])
    FxSpell = recordclass('FxSpell', ['cost', 'remain_prop', 'turns', 'func'])

    def spell_mm(_: Player, b: Boss) -> None:
        b.hit_points -= 4

    def spell_d(p: Player, b: Boss) -> None:
        b.hit_points -= 2
        p.hit_points += 2

    def fxspell_s(_: Player, __: Boss) -> None:
        pass

    def fxspell_p(_: Player, b: Boss) -> None:
        b.hit_points -= 3

    def fxspell_r(p: Player, _: Boss) -> None:
        p.mana_points += 101

    dict_spells = {
        'Magic Missile': Spell(53, spell_mm),
        'Drain': Spell(73, spell_d),
        'Shield': FxSpell(113, 'shield_remain', 6, fxspell_s),
        'Poison': FxSpell(173, 'poison_remain', 6, fxspell_p),
        'Recharge': FxSpell(229, 'recharge_remain', 5, fxspell_r)
    }

    # noinspection PyTypeChecker
    def try_scenario(player: Player, boss: Boss, scenario: Iterator[str], spells: Dict[str, Union[Spell, FxSpell]]) \
            -> int:
        def is_it_finished() -> bool:
            if player.hit_points <= 0:
                raise ValueError
            if boss.hit_points <= 0:
                return True
            return False

        playing = (player, boss)

        while True:
            for s in spells.values():
                if isinstance(s, FxSpell):
                    if getattr(player, s.remain_prop) > 0:
                        s.func(player, boss)
                        setattr(player, s.remain_prop, getattr(player, s.remain_prop) - 1)

            if is_it_finished():
                return player.mana_spent

            if playing[0] == player:
                cur_spell = spells[next(scenario)]

                if player.mana_points < cur_spell.cost:
                    raise ValueError

                if isinstance(cur_spell, Spell):
                    cur_spell.func(player, boss)
                    player.mana_points -= cur_spell.cost
                    player.mana_spent += cur_spell.cost
                else:
                    if getattr(player, cur_spell.remain_prop) > 0:
                        raise ValueError
                    setattr(player, cur_spell.remain_prop, cur_spell.turns)
                    player.mana_points -= cur_spell.cost
                    player.mana_spent += cur_spell.cost
            else:
                player.hit_points -= boss.damage - (7 if player.shield_remain > 0 else 0)

            playing = playing[::-1]
            if is_it_finished():
                return player.mana_spent

    T = TypeVar('T')

    def random_choice_generator(l: List[T]) -> T:
        while True:
            yield random.choice(l)

    # def scenarios(spells: Dict[str, Union[Spell, FxSpell]], max_mana: int):
    #     n_repeat = math.ceil(max_mana / min([s.cost for s in spells.values()]))
    #
    #     prod = []
    #     for n in range(1, n_repeat):
    #         prod.extend(product([(k, v.cost) for k, v in spells.items()], repeat=n))
    #
    #     filtered = [list(map(lambda t: t[1], p)) for p in prod if sum([s[1] for s in p]) <= max_mana]
    #     return sorted(filtered, key=sum)

    least_mana = math.inf
    while True:
        try:
            mana_spent = try_scenario(copy(base_player), copy(base_boss), random_choice_generator(
                ['Magic Missile', 'Drain', 'Shield', 'Poison', 'Recharge']), dict_spells)
            if min(least_mana, mana_spent) < least_mana:
                least_mana = min(least_mana, mana_spent)
                print(least_mana)
        except ValueError:
            pass


# noinspection PyPep8Naming
def day22_part2(in_: str) -> None:
    Player = recordclass(
        'Player',
        ['hit_points', 'mana_points', 'mana_spent', 'shield_remain', 'poison_remain', 'recharge_remain'])
    Boss = recordclass('Boss', ['hit_points', 'damage'])

    base_player = Player(50, 500, 0, 0, 0, 0)
    base_boss = Boss(*list(map(lambda x: int(x.split()[-1]), in_.splitlines())))

    Spell = recordclass('Spell', ['cost', 'func'])
    FxSpell = recordclass('FxSpell', ['cost', 'remain_prop', 'turns', 'func'])

    def spell_mm(_: Player, b: Boss) -> None:
        b.hit_points -= 4

    def spell_d(p: Player, b: Boss) -> None:
        b.hit_points -= 2
        p.hit_points += 2

    def fxspell_s(_: Player, __: Boss) -> None:
        pass

    def fxspell_p(_: Player, b: Boss) -> None:
        b.hit_points -= 3

    def fxspell_r(p: Player, _: Boss) -> None:
        p.mana_points += 101

    dict_spells = {
        'Magic Missile': Spell(53, spell_mm),
        'Drain': Spell(73, spell_d),
        'Shield': FxSpell(113, 'shield_remain', 6, fxspell_s),
        'Poison': FxSpell(173, 'poison_remain', 6, fxspell_p),
        'Recharge': FxSpell(229, 'recharge_remain', 5, fxspell_r)
    }

    # noinspection PyTypeChecker
    def try_scenario(player: Player, boss: Boss, scenario: Iterator[str], spells: Dict[str, Union[Spell, FxSpell]]) \
            -> int:
        def is_it_finished() -> bool:
            if player.hit_points <= 0:
                raise ValueError
            if boss.hit_points <= 0:
                return True
            return False

        playing = (player, boss)

        while True:
            if playing[0] == player:
                player.hit_points -= 1
                if is_it_finished():
                    return player.mana_spent

            for s in spells.values():
                if isinstance(s, FxSpell):
                    if getattr(player, s.remain_prop) > 0:
                        s.func(player, boss)
                        setattr(player, s.remain_prop, getattr(player, s.remain_prop) - 1)

            if is_it_finished():
                return player.mana_spent

            if playing[0] == player:
                cur_spell = spells[next(scenario)]

                if player.mana_points < cur_spell.cost:
                    raise ValueError

                if isinstance(cur_spell, Spell):
                    cur_spell.func(player, boss)
                    player.mana_points -= cur_spell.cost
                    player.mana_spent += cur_spell.cost
                else:
                    if getattr(player, cur_spell.remain_prop) > 0:
                        raise ValueError
                    setattr(player, cur_spell.remain_prop, cur_spell.turns)
                    player.mana_points -= cur_spell.cost
                    player.mana_spent += cur_spell.cost
            else:
                player.hit_points -= boss.damage - (7 if player.shield_remain > 0 else 0)

            playing = playing[::-1]
            if is_it_finished():
                return player.mana_spent

    T = TypeVar('T')

    def random_choice_generator(l: List[T]) -> T:
        while True:
            yield random.choice(l)

    # def scenarios(spells: Dict[str, Union[Spell, FxSpell]], max_mana: int):
    #     n_repeat = math.ceil(max_mana / min([s.cost for s in spells.values()]))
    #
    #     prod = []
    #     for n in range(1, n_repeat):
    #         prod.extend(product([(k, v.cost) for k, v in spells.items()], repeat=n))
    #
    #     filtered = [list(map(lambda t: t[1], p)) for p in prod if sum([s[1] for s in p]) <= max_mana]
    #     return sorted(filtered, key=sum)

    least_mana = math.inf
    while True:
        try:
            mana_spent = try_scenario(copy(base_player), copy(base_boss), random_choice_generator(
                ['Magic Missile', 'Drain', 'Shield', 'Poison', 'Recharge']), dict_spells)
            if min(least_mana, mana_spent) < least_mana:
                least_mana = min(least_mana, mana_spent)
                print(least_mana)
        except ValueError:
            pass


def day23_part1(in_: str) -> None:
    reg = {'a': 0, 'b': 0}
    ir = 0
    instructions = in_.splitlines()

    while ir < len(instructions):
        instr, *args = re.split(r'[ ,]+', instructions[ir])

        if instr == 'hlf':
            reg[args[0]] //= 2
            ir += 1
        elif instr == 'tpl':
            reg[args[0]] *= 3
            ir += 1
        elif instr == 'inc':
            reg[args[0]] += 1
            ir += 1
        elif instr == 'jmp':
            ir += int(args[0])
        elif instr == 'jie':
            if reg[args[0]] % 2 == 0:
                ir += int(args[1])
            else:
                ir += 1
        elif instr == 'jio':
            if reg[args[0]] == 1:
                ir += int(args[1])
            else:
                ir += 1

    print(reg['b'])


def day23_part2(in_: str) -> None:
    reg = {'a': 1, 'b': 0}
    ir = 0
    instructions = in_.splitlines()

    while ir < len(instructions):
        instr, *args = re.split(r'[ ,]+', instructions[ir])

        if instr == 'hlf':
            reg[args[0]] //= 2
            ir += 1
        elif instr == 'tpl':
            reg[args[0]] *= 3
            ir += 1
        elif instr == 'inc':
            reg[args[0]] += 1
            ir += 1
        elif instr == 'jmp':
            ir += int(args[0])
        elif instr == 'jie':
            if reg[args[0]] % 2 == 0:
                ir += int(args[1])
            else:
                ir += 1
        elif instr == 'jio':
            if reg[args[0]] == 1:
                ir += int(args[1])
            else:
                ir += 1

    print(reg['b'])


def day24_part1(in_: str) -> None:
    packs = [int(line) for line in in_.splitlines()]
    tot_grp = sum(packs) // 3

    for n in range(len(packs)):
        eligible = [reduce(lambda x, y: x * y, c) for c in combinations(packs, n) if sum(c) == tot_grp]
        if eligible:
            print(min(eligible))
            return


def day24_part2(in_: str) -> None:
    packs = [int(line) for line in in_.splitlines()]
    tot_grp = sum(packs) // 4

    for n in range(len(packs)):
        eligible = [reduce(lambda x, y: x * y, c) for c in combinations(packs, n) if sum(c) == tot_grp]
        if eligible:
            print(min(eligible))
            return


def day25_part1(in_: str) -> None:
    def case_num(row: int, col: int):
        diag_num = row + col - 1
        return sum(range(1, diag_num)) + col

    x, y = (int(x) for x in re.match(
        r'To continue, please consult the code grid in the manual. {2}Enter the code at row (\d+), column (\d+).',
        in_).groups())

    code = 20151125
    for _ in range(1, case_num(x, y)):
        code = (code * 252533) % 33554393
    print(code)
