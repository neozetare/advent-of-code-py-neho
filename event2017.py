# -*- coding: utf-8 -*-


def day1_part1(in_: str) -> None:
    len_ = len(in_)
    print(sum([int(x) for i, x in enumerate(in_) if x == in_[(i + 1) % len_]]))


def day1_part2(in_: str) -> None:
    len_ = len(in_)
    print(sum([int(x) for i, x in enumerate(in_) if x == in_[(i + len_ // 2) % len_]]))
