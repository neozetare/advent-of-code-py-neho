# -*- coding: utf-8 -*-

import copy
import hashlib
import re
import string
from collections import deque, namedtuple, defaultdict
from itertools import combinations, count, cycle, permutations
from queue import PriorityQueue, Queue
from typing import Set, List, Generator, Union, Tuple

from recordclass import recordclass


def day1_part1(in_: str) -> None:
    pos = (0, 0)
    facing = 0  # N = 0, E = 1, S = 2, W = 3
    factors = ((0, -1), (1, 0), (0, 1), (-1, 0))

    for instr in [x.strip() for x in in_.split(',')]:
        facing += 1 if instr[0] == 'R' else -1
        if facing < 0:
            facing = 3
        elif facing > 3:
            facing = 0

        move = int(instr[1:])
        pos = (pos[0] + factors[facing][0] * move,
               pos[1] + factors[facing][1] * move)

    print(abs(pos[0]) + abs(pos[1]))


def day1_part2(in_: str) -> None:
    pos = (0, 0)
    facing = 0  # N = 0, E = 1, S = 2, W = 3
    factors = ((0, -1), (1, 0), (0, 1), (-1, 0))
    visited = {pos}

    for instr in [x.strip() for x in in_.split(',')]:
        facing += 1 if instr[0] == 'R' else -1
        if facing < 0:
            facing = 3
        elif facing > 3:
            facing = 0

        move = int(instr[1:])
        for i in range(1, move + 1):
            pos = (pos[0] + factors[facing][0],
                   pos[1] + factors[facing][1])

            if pos in visited:
                print(abs(pos[0]) + abs(pos[1]))
                return

            visited.add(pos)


def day2_part1(in_: str) -> None:
    cur = 5

    for line in in_.splitlines():
        for c in line:
            if c == 'U' and cur > 3:
                cur -= 3
            elif c == 'D' and cur < 7:
                cur += 3
            elif c == 'L' and cur % 3 != 1:
                cur -= 1
            elif c == 'R' and cur % 3 != 0:
                cur += 1
        print(cur, end='')
    print()


def day2_part2(in_: str) -> None:
    keypad = [[' ', ' ', '1', ' ', ' '],
              [' ', '2', '3', '4', ' '],
              ['5', '6', '7', '8', '9'],
              [' ', 'A', 'B', 'C', ' '],
              [' ', ' ', 'D', ' ', ' ']]
    cur = [2, 0]

    for line in in_.splitlines():
        for c in line:
            try:
                if c == 'U' and keypad[cur[0] - 1][cur[1]] != ' ':
                    if cur[0] == 0:
                        raise IndexError
                    cur[0] -= 1
                elif c == 'D' and keypad[cur[0] + 1][cur[1]] != ' ':
                    cur[0] += 1
                elif c == 'L' and keypad[cur[0]][cur[1] - 1] != ' ':
                    if cur[1] == 0:
                        raise IndexError
                    cur[1] -= 1
                elif c == 'R' and keypad[cur[0]][cur[1] + 1] != ' ':
                    cur[1] += 1
            except IndexError:
                pass
        print(keypad[cur[0]][cur[1]], end='')
    print()


def day3_part1(in_: str) -> None:
    count = 0
    for t in [sorted([int(i) for i in line.split()]) for line in in_.splitlines()]:
        if sum(t[:2]) > t[2]:
            count += 1
    print(count)


def day3_part2(in_: str) -> None:
    linetuples = [[int(i) for i in line.split()] for line in in_.splitlines()]

    count = 0
    for i in range(0, len(linetuples), 3):
        for t in [sorted(t) for t in zip(*linetuples[i:i + 3])]:
            if sum(t[:2]) > t[2]:
                count += 1
    print(count)


def day4_part1(in_: str) -> None:
    r_line = re.compile(r'([a-z]+(?:-[a-z]+)*)-([0-9]+)\[([a-z]+)\]')

    sum_ = 0
    for line in in_.splitlines():
        name, id_, checksum = r_line.match(line).groups()
        name = name.replace('-', '')

        frequencies = {}
        for c in name:
            if c not in frequencies.keys():
                frequencies[c] = name.count(c)
        sorted_ = [c for c, _ in sorted(frequencies.items(), key=lambda x: (len(name) - x[1], x[0]))]
        if ''.join(sorted_[:5]) == checksum:
            sum_ += int(id_)
    print(sum_)


def day4_part2(in_: str) -> None:
    r_line = re.compile(r'([a-z]+(?:-[a-z]+)*)-([0-9]+)\[([a-z]+)\]')

    alphabet = string.ascii_lowercase
    for line in in_.splitlines():
        name, id_, _ = r_line.match(line).groups()
        id_ = int(id_)

        shifted = ''
        for c in name:
            if c == '-':
                shifted += ' '
            else:
                shifted += alphabet[(alphabet.index(c) + id_) % len(alphabet)]
        if shifted == "northpole object storage":
            print(id_)
            return


def day5_part1(in_: str) -> None:
    i = 0
    for _ in range(8):
        hashed = ''
        while not hashed.startswith('00000'):
            i += 1
            hashed = hashlib.md5(f'{in_}{i}'.encode()).hexdigest()
        print(hashed[5], end='')
    print()


def day5_part2(in_: str) -> None:
    password = ['_'] * 8
    i = 0
    while '_' in password:
        hashed = ''
        while not hashed.startswith('00000'):
            i += 1
            hashed = hashlib.md5(f'{in_}{i}'.encode()).hexdigest()
        try:
            ind = int(hashed[5])
            if password[ind] == '_':
                password[ind] = hashed[6]
        except (ValueError, IndexError):
            pass
    print(''.join(password))


def day6_part1(in_: str) -> None:
    for charset in zip(*in_.splitlines()):
        print(max(set(charset), key=charset.count), end='')
    print()


def day6_part2(in_: str) -> None:
    for charset in zip(*in_.splitlines()):
        print(min(set(charset), key=charset.count), end='')
    print()


def day7_part1(in_: str) -> None:
    def has_abba(b: str) -> bool:
        return any(a == d and b == c and a != b for a, b, c, d
                   in zip(b, b[1:], b[2:], b[3:]))

    count = 0
    for line in in_.splitlines():
        blocks = re.split(r'[\[\]]', line)

        if not any([has_abba(b) for b in blocks[::2]]):
            continue

        if any([has_abba(b) for b in blocks[1::2]]):
            continue

        print(line)
        count += 1
    print(count)


def day7_part2(in_: str) -> None:
    def get_bab_from_supernet(b: str) -> Set[str]:
        return set(f'{b}{a}{b}' for a, b, c in zip(b, b[1:], b[2:]) if a == c)

    count = 0
    for line in in_.splitlines():
        blocks = re.split(r'[\[\]]', line)

        babs = set().union(*[get_bab_from_supernet(b) for b in blocks[::2]])
        if any([any([bab in b for bab in babs]) for b in blocks[1::2]]):
            count += 1
    print(count)


def day8_part1(in_: str) -> None:
    r_rect = r'(?P<wide>[0-9]+)x(?P<tall>[0-9]+)'
    r_rotate = r'(?P<type>row|column) (?:x|y)=(?P<place>[0-9]+) by (?P<shift>[0-9]+)'
    r_line = re.compile(rf'(?P<cmd>rect|rotate) (?:{r_rect}|{r_rotate})')

    screen = [[False for __ in range(50)] for _ in range(6)]
    for line in in_.splitlines():
        m = r_line.match(line)

        if m.group('cmd') == 'rect':
            for y in range(int(m.group('tall'))):
                for x in range(int(m.group('wide'))):
                    screen[y][x] = True

        elif m.group('cmd') == 'rotate':
            place = int(m.group('place'))

            if m.group('type') == 'row':
                seq = screen[place]
            else:
                seq = list(zip(*screen))[place]

            deq = deque(seq)
            deq.rotate(int(m.group('shift')))

            if m.group('type') == 'row':
                screen[place] = list(deq)
            else:
                for i, val in enumerate(deq):
                    screen[i][place] = val

    print(sum([sum(row) for row in screen]))


def day8_part2(in_: str) -> None:
    r_rect = r'(?P<wide>[0-9]+)x(?P<tall>[0-9]+)'
    r_rotate = r'(?P<type>row|column) (?:x|y)=(?P<place>[0-9]+) by (?P<shift>[0-9]+)'
    r_line = re.compile(rf'(?P<cmd>rect|rotate) (?:{r_rect}|{r_rotate})')

    screen = [[False for __ in range(50)] for _ in range(6)]
    for line in in_.splitlines():
        m = r_line.match(line)

        if m.group('cmd') == 'rect':
            for y in range(int(m.group('tall'))):
                for x in range(int(m.group('wide'))):
                    screen[y][x] = True

        elif m.group('cmd') == 'rotate':
            place = int(m.group('place'))

            if m.group('type') == 'row':
                seq = screen[place]
            else:
                seq = list(zip(*screen))[place]

            deq = deque(seq)
            deq.rotate(int(m.group('shift')))

            if m.group('type') == 'row':
                screen[place] = list(deq)
            else:
                for i, val in enumerate(deq):
                    screen[i][place] = val

    for row in screen:
        print(*['#' if c else ' ' for c in row], sep='')


def day9_part1(in_: str) -> None:
    s = in_
    length = 0
    while '(' in s:
        par_index = s.index('(')
        length += par_index
        s = s[par_index:]

        par_index = s.index(')')
        left, right = s[1:par_index].split('x')
        left, right = int(left), int(right)
        s = s[par_index + 1:]

        length += len(s[:left]) * right
        s = s[left:]
    print(length + len(s))


def day9_part2(in_: str) -> None:
    def decompressed_length(s: str) -> int:
        length = 0
        while '(' in s:
            par_index = s.index('(')
            length += par_index
            s = s[par_index:]

            par_index = s.index(')')
            left, right = s[1:par_index].split('x')
            left, right = int(left), int(right)
            s = s[par_index + 1:]

            length += decompressed_length(s[:left]) * right
            s = s[left:]
        return length + len(s)

    print(decompressed_length(in_))


def day10_part1(in_: str) -> None:
    def parse_line(line: str) -> dict:
        d = {}
        split = line.split()
        if split[0] == 'value':
            d['type'] = 'input'
            d['value'] = int(split[1])
            d['dest'] = {'type': 'bot', 'id': int(split[5])}
        else:
            d['type'] = 'give'
            d['src'] = {'type': 'bot', 'id': int(split[1])}
            d['low_dest'] = {'type': split[5], 'id': int(split[6])}
            d['high_dest'] = {'type': split[10], 'id': int(split[11])}
        return d

    instructions = [parse_line(line) for line in in_.splitlines()]

    stock = {}
    while instructions:
        for instr in instructions[:]:
            if instr['type'] == 'input':
                key = f"bot{instr['dest']['id']}"
                try:
                    stock[key].add(instr['value'])
                except KeyError:
                    stock[key] = {instr['value']}
                instructions.remove(instr)
            else:
                key = f"bot{instr['src']['id']}"
                try:
                    if len(stock[key]) == 2:
                        if stock[key] == {61, 17}:
                            print(instr['src']['id'])
                            return

                        key_low = f"{instr['low_dest']['type']}{instr['low_dest']['id']}"
                        try:
                            stock[key_low].add(min(stock[key]))
                        except KeyError:
                            stock[key_low] = {min(stock[key])}

                        key_high = f"{instr['high_dest']['type']}{instr['high_dest']['id']}"
                        try:
                            stock[key_high].add(max(stock[key]))
                        except KeyError:
                            stock[key_high] = {max(stock[key])}

                        instructions.remove(instr)
                        del stock[key]
                except KeyError:
                    pass


def day10_part2(in_: str) -> None:
    def parse_line(line: str) -> dict:
        d = {}
        split = line.split()
        if split[0] == 'value':
            d['type'] = 'input'
            d['value'] = int(split[1])
            d['dest'] = {'type': 'bot', 'id': int(split[5])}
        else:
            d['type'] = 'give'
            d['src'] = {'type': 'bot', 'id': int(split[1])}
            d['low_dest'] = {'type': split[5], 'id': int(split[6])}
            d['high_dest'] = {'type': split[10], 'id': int(split[11])}
        return d

    instructions = [parse_line(line) for line in in_.splitlines()]

    stock = {}
    while instructions:
        for instr in instructions[:]:
            if instr['type'] == 'input':
                key = f"bot{instr['dest']['id']}"
                try:
                    stock[key].add(instr['value'])
                except KeyError:
                    stock[key] = {instr['value']}
                instructions.remove(instr)
            else:
                key = f"bot{instr['src']['id']}"
                try:
                    if len(stock[key]) == 2:
                        key_low = f"{instr['low_dest']['type']}{instr['low_dest']['id']}"
                        try:
                            stock[key_low].add(min(stock[key]))
                        except KeyError:
                            stock[key_low] = {min(stock[key])}

                        key_high = f"{instr['high_dest']['type']}{instr['high_dest']['id']}"
                        try:
                            stock[key_high].add(max(stock[key]))
                        except KeyError:
                            stock[key_high] = {max(stock[key])}

                        instructions.remove(instr)
                        del stock[key]
                except KeyError:
                    pass
    print(sum(stock['output0']) * sum(stock['output1']) * sum(stock['output2']))


def day11_part1(in_: str) -> None:
    GENERATOR, MICROCHIP = 'generator', 'microchip'
    Movable = namedtuple('Movable', ['element', 'type'])
    World = recordclass('World', ['floors', 'elevator'])
    Move = namedtuple('Move', ['movables', 'n_floor'])

    def is_world_safe(world: World) -> bool:
        for floor in world.floors:
            for microchip in [m for m in floor if m.type == MICROCHIP]:
                if Movable(microchip.element, GENERATOR) in floor:
                    continue
                if any([m.element != microchip.element for m in floor if m.type == GENERATOR]):
                    return False
        return True

    def execution(base_world: World) -> int:
        frontier = PriorityQueue()
        frontier.put((1, base_world, 0))
        stamps = [copy.deepcopy(base_world)]

        while not frontier.empty():
            cost, world, n_steps = frontier.get()

            if all([len(floor) == 0 for floor in world.floors[:-1]]):
                return n_steps

            cur_floor = world.floors[world.elevator]

            n_floors = []
            if world.elevator > 0:
                n_floors.append(world.elevator - 1)
            if world.elevator < len(world.floors) - 1:
                n_floors.append(world.elevator + 1)

            moves = []
            for n_floor in n_floors:
                moves += [Move([x], n_floor) for x in cur_floor]
                moves += [Move(list(c), n_floor) for c in combinations(cur_floor, 2)]

            for move in moves:
                tmp_world = copy.deepcopy(world)

                for movable in move.movables:
                    tmp_world.floors[tmp_world.elevator].remove(movable)
                    tmp_world.floors[move.n_floor].add(movable)
                tmp_world.elevator = move.n_floor

                if is_world_safe(tmp_world) and tmp_world not in stamps:
                    new_cost = 1
                    if (len(move.movables) == 2 and tmp_world.elevator > world.elevator) or \
                            (len(move.movables) == 1 and tmp_world.elevator < world.elevator):
                        new_cost = 0
                    frontier.put((new_cost, tmp_world, n_steps + 1))
                    stamps.append(copy.deepcopy(tmp_world))

    r_movable = re.compile(r'([a-z]+)(?:-compatible)? (generator|microchip)')
    floors = []
    for line in in_.splitlines():
        floors.append(set(map(lambda x: Movable(*x), r_movable.findall(line))))
    floors[0].update({Movable('elerium', GENERATOR), Movable('elerium', MICROCHIP),
                      Movable('dilithium', GENERATOR), Movable('dilithium', MICROCHIP)})
    base_world = World(floors, 0)

    print(execution(base_world))


def day11_part2(in_: str) -> None:
    GENERATOR, MICROCHIP = 'generator', 'microchip'
    Movable = namedtuple('Movable', ['element', 'type'])
    World = recordclass('World', ['floors', 'elevator'])
    FrozenWorld = namedtuple('FrozenWorld', ['floors', 'elevator'])

    def as_frozen(world: World) -> FrozenWorld:
        return FrozenWorld(tuple(frozenset(floor) for floor in world.floors), world.elevator)

    def as_mutable(world: FrozenWorld) -> World:
        return World([set(floor) for floor in world.floors], world.elevator)

    def goal(world: World) -> bool:
        return all([len(floor) == 0 for floor in world.floors[:-1]])

    def is_world_safe(world: Union[World, FrozenWorld]) -> bool:
        for floor in world.floors:
            for microchip in [m for m in floor if m.type == MICROCHIP]:
                if Movable(microchip.element, GENERATOR) in floor:
                    continue
                if any([m.element != microchip.element for m in floor if m.type == GENERATOR]):
                    return False
        return True

    def neighbors(world: Union[World, FrozenWorld]) -> Generator[FrozenWorld, None, None]:
        world = world if isinstance(world, World) else as_mutable(world)
        cur_floor = world.floors[world.elevator]

        n_floors = []
        if world.elevator > 0:
            n_floors.append(world.elevator - 1)
        if world.elevator < len(world.floors) - 1:
            n_floors.append(world.elevator + 1)

        moves = []
        for n_floor in n_floors:
            moves += [([x], n_floor) for x in cur_floor]
            moves += [(list(c), n_floor) for c in combinations(cur_floor, 2)]

        for move in moves:
            next_ = copy.deepcopy(world)
            next_.elevator = move[1]
            for movable in move[0]:
                next_.floors[world.elevator].remove(movable)
                next_.floors[next_.elevator].add(movable)

            if is_world_safe(next_):
                yield (as_frozen(next_), move[0])

    def heuristic(next_: World, current: World, moved: List[Movable]) -> int:
        sum_ = sum((len(next_.floors) - i - 1) * 2 * max(0, len(floor) - 1)
                   for i, floor in enumerate(next_.floors))
        if (len(moved) == 2 and next_.elevator > current.elevator) or \
                (len(moved) == 1 and next_.elevator < current.elevator):
            return sum_
        return sum_ + 2

    def search(world: FrozenWorld) -> int:
        frontier = PriorityQueue()
        frontier.put((0, world))
        came_from = {world: None}
        cost_so_far = {world: 0}

        while not frontier.empty():
            _, current = frontier.get()

            if goal(current):
                return cost_so_far[current]

            for next_, moved in neighbors(current):
                new_cost = cost_so_far[current] + 1
                if next_ not in cost_so_far or new_cost < cost_so_far[next_]:
                    cost_so_far[next_] = new_cost
                    priority = new_cost + heuristic(next_, current, moved)
                    frontier.put((priority, next_))
                    came_from[next_] = current

        return 0

    r_movable = re.compile(r'([a-z]+)(?:-compatible)? (generator|microchip)')
    floors = []
    for line in in_.splitlines():
        floors.append(set(map(lambda x: Movable(*x), r_movable.findall(line))))
    floors[0].update({Movable('elerium', GENERATOR), Movable('elerium', MICROCHIP),
                      Movable('dilithium', GENERATOR), Movable('dilithium', MICROCHIP)})
    base_world = World(floors, 0)

    print(search(as_frozen(base_world)))


def day12_part1(in_: str) -> None:
    instructions = []
    for instr, *args in map(lambda s: s.split(), in_.splitlines()):
        instructions.append((instr, args))

    reg = defaultdict(lambda: 0)

    def lit_or_reg_val(arg: str):
        try:
            return int(arg)
        except ValueError:
            return reg[arg]

    while reg['ir'] < len(instructions):
        instr = instructions[reg['ir']]

        if instr[0] == 'cpy':
            reg[instr[1][1]] = lit_or_reg_val(instr[1][0])
        elif instr[0] == 'inc':
            reg[instr[1][0]] += 1
        elif instr[0] == 'dec':
            reg[instr[1][0]] -= 1
        elif instr[0] == 'jnz':
            if lit_or_reg_val(instr[1][0]) != 0:
                reg['ir'] += int(instr[1][1]) - 1

        reg['ir'] += 1
    print(reg['a'])


def day12_part2(in_: str) -> None:
    instructions = []
    for instr, *args in map(lambda s: s.split(), in_.splitlines()):
        instructions.append((instr, args))

    reg = defaultdict(lambda: 0, c=1)

    def lit_or_reg_val(arg: str):
        try:
            return int(arg)
        except ValueError:
            return reg[arg]

    while reg['ir'] < len(instructions):
        instr = instructions[reg['ir']]

        if instr[0] == 'cpy':
            reg[instr[1][1]] = lit_or_reg_val(instr[1][0])
        elif instr[0] == 'inc':
            reg[instr[1][0]] += 1
        elif instr[0] == 'dec':
            reg[instr[1][0]] -= 1
        elif instr[0] == 'jnz':
            if lit_or_reg_val(instr[1][0]) != 0:
                reg['ir'] += int(instr[1][1]) - 1

        reg['ir'] += 1
    print(reg['a'])


def day13_part1(in_: str) -> None:
    fav_num = int(in_)

    Coord = Tuple[int, int]

    def is_open(x: int, y: int) -> bool:
        return bin(x*x + 3*x + 2*x*y + y + y*y + fav_num).count('1') % 2 == 0

    def neighbors(coord: Coord) -> Generator[Coord, None, None]:
        x, y = coord
        for next_ in [(x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)]:
            if next_[0] >= 0 and next_[1] >= 0 and is_open(*next_):
                yield next_

    def heuristic(a: Coord, b: Coord):
        (x1, y1) = a
        (x2, y2) = b
        return abs(x1 - x2) + abs(y1 - y2)

    def search(start: Coord, goal: Coord):
        frontier = PriorityQueue()
        frontier.put((0, start))
        came_from = {start: None}
        cost_so_far = {start: 0}

        while not frontier.empty():
            _, current = frontier.get()

            if current == goal:
                break

            for next_ in neighbors(current):
                new_cost = cost_so_far[current] + 1
                if next_ not in cost_so_far or new_cost < cost_so_far[next_]:
                    cost_so_far[next_] = new_cost
                    priority = new_cost + heuristic(goal, next_)
                    frontier.put((priority, next_))
                    came_from[next_] = current

        return came_from

    start, end = (1, 1), (31, 39)
    preds = search(start, end)

    len_ = 0
    tmp = end
    while tmp != start:
        tmp = preds[tmp]
        len_ += 1
    print(len_)


def day13_part2(in_: str) -> None:
    fav_num = int(in_)

    Coord = Tuple[int, int]

    def is_open(x: int, y: int) -> bool:
        return bin(x*x + 3*x + 2*x*y + y + y*y + fav_num).count('1') % 2 == 0

    def neighbors(coord: Coord) -> Generator[Coord, None, None]:
        x, y = coord
        for next_ in [(x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)]:
            if next_[0] >= 0 and next_[1] >= 0 and is_open(*next_):
                yield next_

    def search(start: Coord):
        frontier = Queue()
        frontier.put(start)
        cost_so_far = {start: 0}

        while not frontier.empty():
            current = frontier.get()

            if cost_so_far[current] == 50:
                continue

            for next_ in neighbors(current):
                new_cost = cost_so_far[current] + 1
                if next_ not in cost_so_far or new_cost < cost_so_far[next_]:
                    cost_so_far[next_] = new_cost
                    frontier.put(next_)

        return cost_so_far

    print(len(search((1, 1)).items()))


def day14_part1(in_: str) -> None:
    salt = in_

    possible_keys = deque(maxlen=1000)
    keys = []
    i = 0
    while len(keys) < 64 or i - keys[-1] <= 1000:
        md5 = hashlib.md5(f'{salt}{i}'.encode()).hexdigest().lower()

        for possible_key, five in possible_keys:
            if five is not None and possible_key not in keys and five in md5:
                keys.append(possible_key)

        tmp = None
        for chars in zip(md5, md5[1:], md5[2:]):
            if len(set(chars)) == 1:
                tmp = chars[0] * 5
                break
        possible_keys.append((i, tmp))

        i += 1
    print(sorted(keys)[63])


def day14_part2(in_: str) -> None:
    salt = in_

    possible_keys = deque(maxlen=1000)
    keys = []
    i = 0
    while len(keys) < 64 or i - keys[-1] <= 1000:
        md5 = hashlib.md5(f'{salt}{i}'.encode()).hexdigest().lower()
        for _ in range(2016):
            md5 = hashlib.md5(md5.encode()).hexdigest().lower()

        for possible_key, five in possible_keys:
            if five is not None and possible_key not in keys and five in md5:
                keys.append(possible_key)

        tmp = None
        for chars in zip(md5, md5[1:], md5[2:]):
            if len(set(chars)) == 1:
                tmp = chars[0] * 5
                break
        possible_keys.append((i, tmp))

        i += 1
    print(sorted(keys)[63])


def day15_part1(in_: str) -> None:
    r_line = re.compile(r'Disc #[0-9]+ has ([0-9]+) positions; at time=0, it is at position ([0-9]+).')

    discs = []
    for line in in_.splitlines():
        m = r_line.match(line)
        discs.append([int(s) for s in m.groups()])

    for i in count():
        if all([(d[1] + i + j) % d[0] == 0 for j, d in enumerate(discs, 1)]):
            print(i)
            return


def day15_part2(in_: str) -> None:
    r_line = re.compile(r'Disc #[0-9]+ has ([0-9]+) positions; at time=0, it is at position ([0-9]+).')

    discs = []
    for line in in_.splitlines():
        m = r_line.match(line)
        discs.append([int(s) for s in m.groups()])
    discs.append([11, 0])

    for i in count():
        if all([(d[1] + i + j) % d[0] == 0 for j, d in enumerate(discs, 1)]):
            print(i)
            return


def day16_part1(in_: str) -> None:
    FILL_LEN = 272
    data = in_
    while len(data) < FILL_LEN:
        # noinspection PyUnresolvedReferences
        data = f"{data}0{''.join('1' if c == '0' else '0' for c in data[::-1])}"

    check = data[:FILL_LEN]
    while True:
        check = ''.join('1' if c1 == c2 else '0' for c1, c2 in zip(check[::2], check[1::2]))
        if len(check) % 2 == 1:
            break
    print(check)


def day16_part2(in_: str) -> None:
    FILL_LEN = 35651584
    data = in_
    while len(data) < FILL_LEN:
        # noinspection PyUnresolvedReferences
        data = f"{data}0{''.join('1' if c == '0' else '0' for c in data[::-1])}"

    check = data[:FILL_LEN]
    while True:
        check = ''.join('1' if c1 == c2 else '0' for c1, c2 in zip(check[::2], check[1::2]))
        if len(check) % 2 == 1:
            break
    print(check)


def day17_part1(in_: str) -> None:
    Node = Tuple[int, int]

    def neighbors(x: int, y: int, hash_key: str):
        hashed = hashlib.md5(hash_key.encode()).hexdigest()
        for h, d, next_ in zip(hashed[:4], ('U', 'D', 'L', 'R'), ((x, y - 1), (x, y + 1), (x - 1, y), (x + 1, y))):
            if next_[0] in range(4) and next_[1] in range(4) and h in ('b', 'c', 'd', 'e', 'f'):
                yield next_, d

    def search(start: Node, goal: Node, base_hash: str) -> str:
        frontier = Queue()
        frontier.put((start, ''))

        while not frontier.empty():
            current, path = frontier.get()

            if current == goal:
                return path

            # noinspection PyTypeChecker
            for next_, direction in neighbors(*current, f'{base_hash}{path}'):
                frontier.put((next_, f'{path}{direction}'))

    print(search((0, 0), (3, 3), in_))


def day17_part2(in_: str) -> None:
    Node = Tuple[int, int]

    def neighbors(x: int, y: int, hash_key: str):
        hashed = hashlib.md5(hash_key.encode()).hexdigest()
        for h, d, next_ in zip(hashed[:4], ('U', 'D', 'L', 'R'), ((x, y - 1), (x, y + 1), (x - 1, y), (x + 1, y))):
            if next_[0] in range(4) and next_[1] in range(4) and ord(h) > ord('a'):
                yield next_, d

    def search(start: Node, goal: Node, base_hash: str) -> int:
        frontier = Queue()
        frontier.put((start, ''))

        longest = 0
        while not frontier.empty():
            current, path = frontier.get()

            if current == goal:
                longest = max(len(path), longest)
                continue

            # noinspection PyTypeChecker
            for next_, direction in neighbors(*current, f'{base_hash}{path}'):
                frontier.put((next_, f'{path}{direction}'))
        return longest

    print(search((0, 0), (3, 3), in_))


def day18_part1(in_: str) -> None:
    row = in_

    sum_ = row.count('.')
    for _ in range(40 - 1):
        next_ = ''
        for left, right in zip(f'.{row[:-1]}', f'{row[1:]}.'):
            next_ += '^' if left != right else '.'
        row = next_
        sum_ += row.count('.')
    print(sum_)


def day18_part2(in_: str) -> None:
    row = in_

    sum_ = row.count('.')
    for _ in range(400000 - 1):
        next_ = ''
        for left, right in zip(f'.{row[:-1]}', f'{row[1:]}.'):
            next_ += '^' if left != right else '.'
        row = next_
        sum_ += row.count('.')
    print(sum_)


def day19_part1(in_: str) -> None:
    elves = deque([i for i in range(1, int(in_) + 1)])
    while len(elves) > 1:
        del elves[1]
        elves.rotate(-1)
    print(elves[0])


def day19_part2(in_: str) -> None:
    class Elf:
        def __init__(self, ind):
            self.ind = ind
            self.right = None
            self.left = None

        def remove(self):
            self.left.right = self.right
            self.right.left = self.left

    n_elves = int(in_)
    elves = []
    for i in range(1, n_elves + 1):
        elves.append(Elf(i))
        try:
            elves[-1].left = elves[-2]
            elves[-2].right = elves[-1]
        except IndexError:
            pass
    elves[0].left = elves[-1]
    elves[-1].right = elves[0]

    cur = elves[0]
    to_del = elves[n_elves // 2]

    jump = n_elves % 2 == 1
    while cur != to_del:
        to_del.remove()
        cur = cur.right
        to_del = to_del.right.right if jump else to_del.right
        jump = not jump

    print(cur.ind)


def day20_part1(in_: str) -> None:
    ranges = [(int(a), int(b)) for a, b in [line.split('-') for line in in_.splitlines()]]

    def is_valid(n: int):
        if not (0 <= n < 2 ** 32):
            return False
        for a, b in ranges:
            if a <= n <= b:
                return False
        return True

    candidates = [r[1] + 1 for r in ranges]
    valids = filter(is_valid, candidates)
    print(min(valids))


def day20_part2(in_: str) -> None:
    ranges = [(int(a), int(b)) for a, b in [line.split('-') for line in in_.splitlines()]]

    def is_valid(n: int):
        if not (0 <= n < 2 ** 32):
            return False
        for a, b in ranges:
            if a <= n <= b:
                return False
        return True

    candidates = [r[1] + 1 for r in ranges]
    valids = list(filter(is_valid, candidates))
    print(len(valids))


def day21_part1(in_: str) -> None:
    password = 'abcdefgh'

    for line in in_.splitlines():
        splitted = line.split(' ')

        if line.startswith('swap position'):
            x, y = int(splitted[2]), int(splitted[5])
            listword = list(password)
            listword[x], listword[y] = listword[y], listword[x]
            password = ''.join(listword)

        elif line.startswith('swap letter'):
            x, y = splitted[2], splitted[5]
            password = password.replace(x, '·').replace(y, x).replace('·', y)

        elif line.startswith('rotate left') or line.startswith('rotate right'):
            direction, distance = splitted[1], int(splitted[2]) % len(password)
            if direction == 'left':
                password = password[distance:] + password[:distance]
            else:
                password = password[-distance:] + password[:-distance]

        elif line.startswith('rotate based'):
            x = splitted[6]
            ind = password.index(x)
            distance = ind + 2 if ind >= 4 else ind + 1
            distance %= len(password)
            password = password[-distance:] + password[:-distance]

        elif line.startswith('reverse'):
            x, y = int(splitted[2]), int(splitted[4])
            assert x < y
            password = password[:x] + password[x:y+1][::-1] + password[y+1:]

        elif line.startswith('move'):
            x, y = int(splitted[2]), int(splitted[5])
            letter = password[x]
            password = password[:x] + password[x+1:]
            password = password[:y] + letter + password[y:]

        else:
            assert False

    print(''.join(password))


def day21_part2(in_: str) -> None:
    scrambled = 'fbgdceah'

    for password in permutations(scrambled):
        password = ''.join(password)
        start = password

        for line in in_.splitlines():
            splitted = line.split(' ')

            if line.startswith('swap position'):
                x, y = int(splitted[2]), int(splitted[5])
                listword = list(password)
                listword[x], listword[y] = listword[y], listword[x]
                password = ''.join(listword)

            elif line.startswith('swap letter'):
                x, y = splitted[2], splitted[5]
                password = password.replace(x, '·').replace(y, x).replace('·', y)

            elif line.startswith('rotate left') or line.startswith('rotate right'):
                direction, distance = splitted[1], int(splitted[2]) % len(password)
                if direction == 'left':
                    password = password[distance:] + password[:distance]
                else:
                    password = password[-distance:] + password[:-distance]

            elif line.startswith('rotate based'):
                x = splitted[6]
                ind = password.index(x)
                distance = ind + 2 if ind >= 4 else ind + 1
                distance %= len(password)
                password = password[-distance:] + password[:-distance]

            elif line.startswith('reverse'):
                x, y = int(splitted[2]), int(splitted[4])
                assert x < y
                password = password[:x] + password[x:y+1][::-1] + password[y+1:]

            elif line.startswith('move'):
                x, y = int(splitted[2]), int(splitted[5])
                letter = password[x]
                password = password[:x] + password[x+1:]
                password = password[:y] + letter + password[y:]

            else:
                assert False

        if password == scrambled:
            print(start)
            return


def day22_part1(in_: str) -> None:
    r_line = re.compile(r'/dev/grid/node-x([0-9]+)-y([0-9]+) +([0-9]+)T +([0-9]+)T +([0-9]+)T.+')
    Node = namedtuple('Node', ['size', 'used', 'avail'])

    nodes = []
    for line in in_.splitlines()[2:]:
        _, _, *node_values = [int(i) for i in r_line.match(line).groups()]
        nodes.append(Node(*node_values))

    n_viables = 0
    for a, b in permutations(nodes, 2):
        if a.used != 0 and a.used <= b.avail:
            n_viables += 1

    print(n_viables)


def day22_part2(in_: str) -> None:
    in_ = """root@ebhq-gridcenter# df -h
Filesystem            Size  Used  Avail  Use%
/dev/grid/node-x0-y0   10T    8T     2T   80%
/dev/grid/node-x0-y1   11T    6T     5T   54%
/dev/grid/node-x0-y2   32T   28T     4T   87%
/dev/grid/node-x1-y0    9T    7T     2T   77%
/dev/grid/node-x1-y1    8T    0T     8T    0%
/dev/grid/node-x1-y2   11T    7T     4T   63%
/dev/grid/node-x2-y0   10T    6T     4T   60%
/dev/grid/node-x2-y1    9T    8T     1T   88%
/dev/grid/node-x2-y2    9T    6T     3T   66%"""

    r_line = re.compile(r'/dev/grid/node-x([0-9]+)-y([0-9]+) +([0-9]+)T +([0-9]+)T. +([0-9]+)T+')

    class Node:
        def __init__(self, size: int, used: int, neighbors: List['Node'] = []):
            self._size = 0
            self._used = 0
            self._neighbors = []

            self.size = size
            self.used = used
            self._neighbors = neighbors

        @property
        def size(self) -> int:
            return self._size

        @size.setter
        def size(self, value: int):
            assert isinstance(value, int)
            assert value >= 0
            assert value >= self.used
            self._size = value

        @property
        def used(self) -> int:
            return self._used

        @used.setter
        def used(self, value: int):
            assert isinstance(value, int)
            assert value >= 0
            assert value <= self.size
            self._used = value

        @property
        def avail(self) -> int:
            return self.size - self.used

        @avail.setter
        def avail(self, value: int):
            assert isinstance(value, int)
            self.used = self.size - value

        @property
        def neighbors(self) -> List['Node']:
            return self._neighbors

        def __repr__(self):
            return f'Node(size={self.size}, used={self.used})'

    # gridcenter = []
    # for line in in_.splitlines()[2:]:
    #     x, y, *node_values = [int(i) for i in r_line.match(line).groups()]
    #     node = Node(*node_values)
    #     while len(gridcenter) <= x:
    #         gridcenter.append([])
    #     while len(gridcenter[x]) <= y:
    #         gridcenter[x].append([])
    #     gridcenter[x][y] = node
    #
    # dimensions = (len(gridcenter), len(gridcenter[0]))
    #
    # Vertex = recordclass('Vertex', ['grid', 'depth', 'start'])
    #
    # visited, queue = set(), deque([Vertex(gridcenter, 0, (dimensions[0] - 1, 0))])
    # while queue:
    #     current = queue.popleft()
    #     x, y = current.start
    #
    #     neighbors = []
    #
    #     data = current.grid[x][y].used
    #     if x > 0 and current.grid[x - 1][y].avail >= data:
    #         newgrid = copy.deepcopy(current.grid)
    #         newgrid[x][y].used = 0
    #         newgrid[x][y].avail = newgrid[x][y].size
    #         newgrid[x - 1][y].avail -= data
    #         newgrid[x - 1][y].used += data
    #         neighbors.append(Vertex(newgrid, 1, (x - 1, y)))
