# -*- coding: utf-8 -*-
import copy
import datetime
import math
import re
from collections import defaultdict, OrderedDict, deque
from functools import reduce
from itertools import cycle, count
from typing import List, Tuple

from recordclass import recordclass


def day1_part1(in_: str) -> None:
    print(sum([int(line) for line in in_.splitlines()]))


def day1_part2(in_: str) -> None:
    sum_ = 0
    seen = {0}
    for i in cycle([int(line) for line in in_.splitlines()]):
        sum_ += i
        if sum_ in seen:
            break
        seen.add(sum_)
    print(sum_)


def day2_part1(in_: str) -> None:
    twos, threes = 0, 0
    for s in in_.splitlines():
        set_ = set(s)
        twos += int(any([s.count(c) == 2 for c in set_]))
        threes += int(any([s.count(c) == 3 for c in set_]))
    print(twos * threes)


def day2_part2(in_: str) -> None:
    seen = set()
    for s1 in in_.splitlines():
        for s2 in seen:
            if sum([1 for c1, c2 in zip(s1, s2) if c1 != c2]) == 1:
                print(''.join([c1 for c1, c2 in zip(s1, s2) if c1 == c2]))
                return
        seen.add(s1)


def day3_part1(in_: str) -> None:
    fabric = [[0 for __ in range(1000)] for _ in range(1000)]

    r_line = re.compile(r'#[0-9]+ @ ([0-9]+),([0-9]+): ([0-9]+)x([0-9]+)')
    for line in in_.splitlines():
        margin_left, margin_top, width, height = map(int, r_line.match(line).groups())
        for y in range(margin_top, margin_top + height):
            for x in range(margin_left, margin_left + width):
                fabric[y][x] += 1

    print(sum([sum([x >= 2 for x in row]) for row in fabric]))


def day3_part2(in_: str) -> None:
    fabric = [[0 for __ in range(1000)] for _ in range(1000)]

    r_line = re.compile(r'#([0-9]+) @ ([0-9]+),([0-9]+): ([0-9]+)x([0-9]+)')
    lines = [list(map(int, r_line.match(line).groups())) for line in in_.splitlines()]

    for line in lines:
        _, margin_left, margin_top, width, height = line
        for y in range(margin_top, margin_top + height):
            for x in range(margin_left, margin_left + width):
                fabric[y][x] += 1

    for line in lines:
        id_, margin_left, margin_top, width, height = line
        if all([all(x == 1 for x in row[margin_left:margin_left + width])
                for row in fabric[margin_top:margin_top + height]]):
            print(id_)
            return


def day4_part1(in_: str) -> None:
    SLEEP, WAKE_UP = -1, -2
    r_line = re.compile(r'\[(?P<datetime>.+)\] (?P<cmd>falls asleep|wakes up|Guard #(?P<id>[0-9]+) begins shift)')

    entries = {}
    for line in in_.splitlines():
        m = r_line.match(line)
        dt = datetime.datetime.strptime(m.group('datetime'), '%Y-%m-%d %H:%M')
        if m.group('cmd')[0] == 'f':
            entries[dt] = SLEEP
        elif m.group('cmd')[0] == 'w':
            entries[dt] = WAKE_UP
        else:
            entries[dt] = int(m.group('id'))

    sorted_dt = sorted(entries.keys())
    assert entries[sorted_dt[0]] >= 0

    sleeps = defaultdict(list)
    guard = -1
    start = None
    for dt, entry in [(dt, entries[dt]) for dt in sorted_dt]:
        if entry == SLEEP:
            start = dt
        elif entry == WAKE_UP:
            assert start is not None
            sleeps[guard].append((start.minute, dt.minute))
        else:
            guard = entry
            start = None

    guard, _ = max([(guard, sum([end - start for start, end in periods]))
                    for guard, periods in sleeps.items()],
                   key=lambda x: x[1])
    minute, _ = max([(minute, sum([1 for start, end in sleeps[guard] if start <= minute < end]))
                     for minute in range(60)],
                    key=lambda x: x[1])
    print(guard * minute)


def day4_part2(in_: str) -> None:
    SLEEP, WAKE_UP = -1, -2
    r_line = re.compile(r'\[(?P<datetime>.+)\] (?P<cmd>falls asleep|wakes up|Guard #(?P<id>[0-9]+) begins shift)')

    entries = {}
    for line in in_.splitlines():
        m = r_line.match(line)
        dt = datetime.datetime.strptime(m.group('datetime'), '%Y-%m-%d %H:%M')
        if m.group('cmd')[0] == 'f':
            entries[dt] = SLEEP
        elif m.group('cmd')[0] == 'w':
            entries[dt] = WAKE_UP
        else:
            entries[dt] = int(m.group('id'))

    sorted_dt = sorted(entries.keys())
    assert entries[sorted_dt[0]] >= 0

    sleeps = defaultdict(list)
    guard = -1
    start = None
    for dt, entry in [(dt, entries[dt]) for dt in sorted_dt]:
        if entry == SLEEP:
            start = dt
        elif entry == WAKE_UP:
            assert start is not None
            sleeps[guard].append((start.minute, dt.minute))
        else:
            guard = entry
            start = None

    guard, minute, _ = max([(guard, minute, sum([1 for start, end in sleeps[guard] if start <= minute < end]))
                            for minute in range(60) for guard in sleeps.keys()],
                           key=lambda x: x[2])

    print(guard * minute)


def day5_part1(in_: str) -> None:
    i = 0
    s = in_
    while i < len(s):
        try:
            if s[i] != s[i + 1] and s[i].upper() == s[i + 1].upper():
                s = s[:i] + s[i + 2:]
                i = max(0, i - 1)
                continue
        except IndexError:
            pass
        i += 1
    print(len(s))


def day5_part2(in_: str) -> None:
    chars = set(in_.upper())

    min_ = math.inf
    for char in chars:
        i = 0
        s = in_.replace(char, '').replace(char.lower(), '')
        while i < len(s):
            try:
                if s[i] != s[i + 1] and s[i].upper() == s[i + 1].upper():
                    s = s[:i] + s[i + 2:]
                    i = max(0, i - 1)
                    continue
            except IndexError:
                pass
            i += 1
        min_ = min(min_, len(s))
    print(min_)


def day6_part1(in_: str) -> None:
    coords = [tuple(map(int, line.split(', '))) for line in in_.splitlines()]
    coords_x, coords_y = list(zip(*coords))
    min_x, max_x = min(coords_x), max(coords_x)
    min_y, max_y = min(coords_y), max(coords_y)

    grid = [[-1 for _ in range(max_x + 1)] for _ in range(max_y + 1)]
    for i, (x, y) in enumerate(coords):
        grid[y][x] = i

    for y in range(min_y, max_y + 1):
        for x in range(min_x, max_x + 1):
            dists = sorted([(abs(x - c[0]) + abs(y - c[1]), i) for i, c in enumerate(coords)])
            if dists[0][0] != dists[1][0]:
                grid[y][x] = dists[0][1]

    coords_areas = defaultdict(lambda: 0)
    coords_areas[-1] = -1
    for (x1, y1), (x2, y2) in ([((x, min_y), (x, max_y)) for x in range(min_x, max_x + 1)] +
                               [((min_x, y), (max_x, y)) for y in range(min_y, max_y + 1)]):
        coords_areas[grid[y1][x1]] = -1
        coords_areas[grid[y2][x2]] = -1

    for y in range(min_y, max_y + 1):
        for x in range(min_x, max_x + 1):
            if coords_areas[grid[y][x]] != -1:
                coords_areas[grid[y][x]] += 1

    print(max(coords_areas.values()))


def day6_part2(in_: str) -> None:
    coords = [tuple(map(int, line.split(', '))) for line in in_.splitlines()]
    coords_x, coords_y = list(zip(*coords))
    min_x, max_x = min(coords_x), max(coords_x)
    min_y, max_y = min(coords_y), max(coords_y)

    grid = [[-1 for _ in range(max_x + 1)] for _ in range(max_y + 1)]
    for i, (x, y) in enumerate(coords):
        grid[y][x] = i

    for y in range(min_y, max_y + 1):
        for x in range(min_x, max_x + 1):
            grid[y][x] = sum([abs(x - c[0]) + abs(y - c[1]) for c in coords])
    print(len([1 for row in grid[min_y:] for cell in row[min_x:] if cell < 10000]))


def day7_part1(in_: str) -> None:
    r_line = re.compile(r'Step ([A-Z]) must be finished before step ([A-Z]) can begin.')

    steps = defaultdict(set)
    for line in in_.splitlines():
        m = r_line.match(line)
        steps[m.group(2)].add(m.group(1))
    # noinspection PyTypeChecker
    for v in reduce(set.union, steps.values()):
        if v not in steps.keys():
            steps[v] = set()
    steps = OrderedDict(sorted(steps.items()))

    while len(steps) > 0:
        for k, v in steps.items():
            if all(x not in steps.keys() for x in v):
                print(k, end='')
                del steps[k]
                break


def day7_part2(in_: str) -> None:
    r_line = re.compile(r'Step ([A-Z]) must be finished before step ([A-Z]) can begin.')

    steps = defaultdict(set)
    for line in in_.splitlines():
        m = r_line.match(line)
        steps[m.group(2)].add(m.group(1))
    # noinspection PyTypeChecker
    for v in reduce(set.union, steps.values()):
        if v not in steps.keys():
            steps[v] = set()
    steps = OrderedDict(sorted(steps.items()))

    workers = [[None, 0] for _ in range(5)]
    sec = 0
    while len(steps) > 0:
        print(workers)
        for w in workers:
            if w[0] is None:
                continue
            w[1] -= 1
            if w[1] == 0:
                del steps[w[0]]
                w[0] = None

        for k, v in steps.items():
            if k not in list(zip(*workers))[0] and all(x not in steps.keys() for x in v):
                for w in workers:
                    if w[0] is None:
                        w[0] = k
                        w[1] = 60 + ord(k) - ord('A') + 1
                        break

        sec += 1
    print(sec - 1)


def day8_part1(in_: str) -> None:
    def get_tree_metadata_sum(data: List[int]) -> Tuple[int, int]:
        n_child, n_metadata = data[:2]
        sum_metadata = 0

        i = 2
        for j in range(n_child):
            sum_, end = get_tree_metadata_sum(data[i:])
            sum_metadata += sum_
            i += end

        sum_metadata += sum(data[i:i + n_metadata])
        return sum_metadata, i + n_metadata

    data = list(map(int, in_.split()))
    sum_, end = get_tree_metadata_sum(data)
    assert end == len(data)
    print(sum_)


def day8_part2(in_: str) -> None:
    def get_tree_value(data: List[int]) -> Tuple[int, int]:
        n_child, n_metadata = data[:2]

        i = 2
        if n_child == 0:
            sum_values = sum(data[i:i + n_metadata])
        else:
            sum_values = 0

            childs_values = [0]
            for j in range(n_child):
                value, end = get_tree_value(data[i:])
                childs_values.append(value)
                i += end

            for j in data[i:i + n_metadata]:
                try:
                    sum_values += childs_values[j]
                except IndexError:
                    pass

        return sum_values, i + n_metadata

    data = list(map(int, in_.split()))
    value, end = get_tree_value(data)
    assert end == len(data)
    print(value)


def day9_part1(in_: str) -> None:
    splitted = in_.split()
    n_players, n_marbles = int(splitted[0]), int(splitted[-2]) + 1

    if n_marbles < 23:
        print(0)
        return

    scores = [0 for _ in range(n_players)]
    circle = deque([0])
    for marble in range(1, n_marbles):
        if marble % 23 == 0:
            circle.rotate(7)
            scores[(marble - 1) % n_players] += marble + circle.pop()
            circle.rotate(-1)
        else:
            circle.rotate(-1)
            circle.append(marble)
    print(max(scores))


def day9_part2(in_: str) -> None:
    splitted = in_.split()
    n_players, n_marbles = int(splitted[0]), (int(splitted[-2]) + 1) * 100

    if n_marbles < 23:
        print(0)
        return

    scores = [0 for _ in range(n_players)]
    circle = deque([0])
    for marble in range(1, n_marbles):
        if marble % 23 == 0:
            circle.rotate(7)
            scores[(marble - 1) % n_players] += marble + circle.pop()
            circle.rotate(-1)
        else:
            circle.rotate(-1)
            circle.append(marble)
    print(max(scores))


def day10_part1(in_: str) -> None:
    LIKE_TOLERANCE = 0.1
    r_line = re.compile(r'-?[0-9]+')

    values = []
    for line in in_.splitlines():
        values.append([int(i) for i in r_line.findall(line)])

    min_ = (0, math.inf)
    for i in count():
        pos_x = [(px + i * vx) for px, py, vx, vy in values]
        pos_y = [(py + i * vy) for px, py, vx, vy in values]
        area = (max(pos_x) - min(pos_x)) * (max(pos_y) - min(pos_y))
        if area < min_[1]:
            min_ = (i, area)
        elif area >= min_[1] * (1 + LIKE_TOLERANCE):
            break

    pos_x = [(px + min_[0] * vx) for px, py, vx, vy in values]
    pos_y = [(py + min_[0] * vy) for px, py, vx, vy in values]
    min_x, min_y = min(pos_x), min(pos_y)
    max_x, max_y = max(pos_x), max(pos_y)

    sky = [[False for _ in range(min_x, max_x + 1)]
           for _ in range(min_y, max_y + 1)]
    for px, py, vx, vy in values:
        sky[py + min_[0] * vy - min_y][px + min_[0] * vx - min_x] = True

    for row in sky:
        for cell in row:
            print('#' if cell else ' ', end='')
        print()


def day10_part2(in_: str) -> None:
    LIKE_TOLERANCE = 0.1
    r_line = re.compile(r'-?[0-9]+')

    values = []
    for line in in_.splitlines():
        values.append([int(i) for i in r_line.findall(line)])

    min_ = (0, math.inf)
    for i in count():
        pos_x = [(px + i * vx) for px, py, vx, vy in values]
        pos_y = [(py + i * vy) for px, py, vx, vy in values]
        area = (max(pos_x) - min(pos_x)) * (max(pos_y) - min(pos_y))
        if area < min_[1]:
            min_ = (i, area)
        elif area >= min_[1] * (1 + LIKE_TOLERANCE):
            print(min_[0])
            break


def day11_part1(in_: str) -> None:
    serial_number = int(in_)

    grid = []
    for y in range(1, 301):
        grid.append([])
        for x in range(1, 301):
            rack_id = x + 10
            power_level = ((rack_id * y + serial_number) * rack_id) // 100 % 10 - 5
            grid[-1].append(power_level)

    total_powers = {}
    for y in range(1, 298):
        for x in range(1, 298):
            total_power = sum(grid[yy - 1][xx - 1] for xx in range(x, x + 3) for yy in range(y, y + 3))
            total_powers[(x, y)] = total_power
    # noinspection PyArgumentList
    print(*max(total_powers.items(), key=lambda x: x[1])[0], sep=',')


def day11_part2(in_: str) -> None:
    GRID_SIZE = 300
    serial_number = int(in_)

    total_powers = {}
    for size in range(1, GRID_SIZE + 1):
        for y in range(1, GRID_SIZE + 1 - size + 1):
            for x in range(1, GRID_SIZE + 1 - size + 1):
                if size == 1:
                    rack_id = x + 10
                    power_level = ((rack_id * y + serial_number) * rack_id) // 100 % 10 - 5
                    total_powers[(size, x, y)] = power_level
                elif size % 2 == 0:
                    half = size // 2
                    total_powers[(size, x, y)] = sum(total_powers[(half, xx, yy)]
                                                     for xx in (x, x + half) for yy in (y, y + half))
                else:
                    half_ceil = size // 2 + 1
                    total_powers[(size, x, y)] = total_powers[(half_ceil, x, y)]\
                        + total_powers[(half_ceil - 1, x + half_ceil, y + half_ceil)]\
                        + total_powers[(half_ceil - 1, x + half_ceil, y + 1)]\
                        + total_powers[(half_ceil - 1, x + 1, y + half_ceil)]\
                        + sum(total_powers[(1, x + i, y)] + total_powers[(1, x, y + i)]
                              for i in range(half_ceil, size))

    # noinspection PyArgumentList
    max_ = max(total_powers.items(), key=lambda x: x[1])[0]
    print(*max_[1:], max_[0], sep=',')


def day12_part1(in_: str) -> None:
    splitted = in_.splitlines()
    r_rule = re.compile(r'([#.]{5}) => ([#.])')

    ind_0 = 0
    state = splitted[0].split()[-1]

    rules = {}
    for line in splitted[2:]:
        m = r_rule.match(line)
        rules[m.group(1)] = m.group(2)

    for _ in range(20):
        state = f'....{state}....'
        next_ = ''
        for i in range(2, len(state) - 2):
            try:
                next_ += rules[state[i - 2:i + 3]]
            except KeyError:
                next_ += '.'

        try:
            ind_0 = ind_0 + 2 - next_.index('#')
            state = next_.strip('.')
        except ValueError:
            state = '.'
            ind_0 = 0

    print(sum(i - ind_0 for i, c in enumerate(state) if c == '#'))


def day12_part2(in_: str) -> None:
    splitted = in_.splitlines()
    r_rule = re.compile(r'([#.]{5}) => ([#.])')

    ind_0 = 0
    state = splitted[0].split()[-1]

    rules = {}
    for line in splitted[2:]:
        m = r_rule.match(line)
        rules[m.group(1)] = m.group(2)

    def state_sum(state: str) -> int:
        return sum(i - ind_0 for i, c in enumerate(state) if c == '#')

    lasts = deque([(-1, state_sum(state), -1)], 10)
    for i in count():
        state = f'....{state}....'
        next_ = ''
        for j in range(2, len(state) - 2):
            try:
                next_ += rules[state[j - 2:j + 3]]
            except KeyError:
                next_ += '.'

        try:
            ind_0 = ind_0 + 2 - next_.index('#')
            state = next_.strip('.')
        except ValueError:
            state = '.'
            ind_0 = 0

        sum_ = state_sum(state)
        lasts.appendleft((i, sum_, sum_ - lasts[0][1]))
        for j in range(len(lasts) - 1):
            if lasts[j][2] != lasts[j + 1][2]:
                break
        else:
            print(lasts[0][1] + lasts[0][2] * (50000000000 - lasts[0][0] - 1))
            return


def day13_part1(in_: str) -> None:
    Cart = recordclass('Cart', ['y', 'x', 'dir', 'mem'])

    tracks = []
    carts = []
    for y, row in enumerate(in_.splitlines()):
        tracks.append([])
        for x, c in enumerate(row):
            if c == '<' or c == '>':
                carts.append(Cart(y, x, c, 0))
                tracks[-1].append('-')
            elif c == '^' or c == 'v':
                carts.append(Cart(y, x, c, 0))
                tracks[-1].append('|')
            else:
                tracks[-1].append(c)

    left = {'^': '<', '<': 'v', 'v': '>', '>': '^'}
    right = {'^': '>', '>': 'v', 'v': '<', '<': '^'}

    while True:
        for c in sorted(carts):
            if c.dir == '^':
                c.y -= 1
            elif c.dir == 'v':
                c.y += 1
            elif c.dir == '<':
                c.x -= 1
            elif c.dir == '>':
                c.x += 1
            else:
                assert False

            if tracks[c.y][c.x] in ('-', '|'):
                continue
            elif tracks[c.y][c.x] == '+':
                if c.mem == 0:
                    c.dir = left[c.dir]
                    c.mem = 1
                elif c.mem == 1:
                    c.mem = 2
                elif c.mem == 2:
                    c.dir = right[c.dir]
                    c.mem = 0
                else:
                    assert False
            elif tracks[c.y][c.x] == '/' and c.dir in ('<', '>')\
                    or tracks[c.y][c.x] == '\\' and c.dir in ('^', 'v'):
                c.dir = left[c.dir]
            elif tracks[c.y][c.x] == '/' and c.dir in ('^', 'v')\
                    or tracks[c.y][c.x] == '\\' and c.dir in ('<', '>'):
                c.dir = right[c.dir]
            else:
                assert False

            if any(c.x == c2.x and c.y == c2.y for c2 in carts if c2 is not c):
                print(c.x, c.y, sep=',')
                return


def day13_part2(in_: str) -> None:
    in_ = r"""/>-<\  
|   |  
| /<+-\
| | | v
\>+</ |
  |   ^
  \<->/"""
    Cart = recordclass('Cart', ['y', 'x', 'dir', 'mem'])

    tracks = []
    carts = []
    for y, row in enumerate(in_.splitlines()):
        tracks.append([])
        for x, c in enumerate(row):
            if c == '<' or c == '>':
                carts.append(Cart(y, x, c, 0))
                tracks[-1].append('-')
            elif c == '^' or c == 'v':
                carts.append(Cart(y, x, c, 0))
                tracks[-1].append('|')
            else:
                tracks[-1].append(c)

    def print_tracks(tracks: List[List[str]]) -> None:
        print_tracks = copy.deepcopy(tracks)
        for c in carts:
            print_tracks[c.y][c.x] = c.dir
        print('\n'.join(''.join(row) for row in print_tracks))

    left = {'^': '<', '<': 'v', 'v': '>', '>': '^'}
    right = {'^': '>', '>': 'v', 'v': '<', '<': '^'}

    print_tracks(tracks)
    while len(carts) > 1:
        for c in sorted(carts):
            # if c not in carts:
            #     continue

            if c.dir == '^':
                c.y -= 1
            elif c.dir == 'v':
                c.y += 1
            elif c.dir == '<':
                c.x -= 1
            elif c.dir == '>':
                c.x += 1
            else:
                assert False

            if tracks[c.y][c.x] in ('-', '|'):
                continue
            elif tracks[c.y][c.x] == '+':
                if c.mem == 0:
                    c.dir = left[c.dir]
                    c.mem = 1
                elif c.mem == 1:
                    c.mem = 2
                elif c.mem == 2:
                    c.dir = right[c.dir]
                    c.mem = 0
                else:
                    assert False
            elif tracks[c.y][c.x] == '/' and c.dir in ('<', '>')\
                    or tracks[c.y][c.x] == '\\' and c.dir in ('^', 'v'):
                c.dir = left[c.dir]
            elif tracks[c.y][c.x] == '/' and c.dir in ('^', 'v')\
                    or tracks[c.y][c.x] == '\\' and c.dir in ('<', '>'):
                c.dir = right[c.dir]
            else:
                assert False

            for c2 in carts:
                if c2 is not c and c.x == c2.x and c.y == c2.y:
                    print(c, c2)
                    carts.remove(c)
                    carts.remove(c2)

        print()
        print_tracks(tracks)
