#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Resolution of "Advent of Code" by Eric Wastl
https://adventofcode.com/
http://was.tl
"""

__author__ = 'Thomas Neozetare Razafindralambo'
__license__ = 'WTFPL 2'
__email__ = 'razathza@gmail.com'

import argparse
import asyncio
import os
import time

# noinspection PyUnresolvedReferences
import event2015
# noinspection PyUnresolvedReferences
import event2016
# noinspection PyUnresolvedReferences
import event2017
# noinspection PyUnresolvedReferences
import event2018
# noinspection PyUnresolvedReferences
import event2019
# noinspection PyUnresolvedReferences
import event2020
# noinspection PyUnresolvedReferences
import event2021

_ROOT = os.path.dirname(os.path.realpath(__file__))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='advent-of-code')
    parser.add_argument('event', metavar='<event>',
                        type=int, choices=range(2015, 2022),
                        help='event (year) of the exercise')
    parser.add_argument('day', metavar='<day>',
                        type=int, choices=range(0, 26),
                        help='day of the exercise')
    parser.add_argument('part', metavar='<part>',
                        type=int, choices=(1, 2),
                        help='part of the exercise')
    parser.add_argument('--timeit', '-t',
                        action='store_true',
                        help='time the execution')
    args = parser.parse_args()

    mod_name = f'event{args.event}'
    func_name = f'day{args.day}_part{args.part}'

    try:
        mod = vars()[mod_name]
    except KeyError:
        raise NameError(f'module {mod_name!r} for event {args.event!r} not found in namespace')
    try:
        func = vars(mod)[func_name]
    except KeyError:
        raise NameError(f'function {func_name!r} for day {args.day!r} part {args.part!r} not found '
                        f'in module {mod_name!r}')

    file_name = f'{args.event}_{args.day:02d}.txt'
    file_path = os.path.join(_ROOT, 'inputs', file_name)

    try:
        with open(file_path) as f:
            start = time.time()
            if asyncio.iscoroutinefunction(func):
                loop = asyncio.new_event_loop()
                asyncio.set_event_loop(loop)
                loop.run_until_complete(func(f.read()))
            else:
                func(f.read())
            if args.timeit:
                print('Runtime:', time.time() - start)
    except FileNotFoundError:
        raise NameError(f'input {file_name!r} for event {args.event!r} day {args.day!r} not found '
                        f'in directory {os.path.join(_ROOT, "inputs")!r}')
